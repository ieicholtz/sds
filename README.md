SDS-Mobile-Detailing
==============

Wordpress Setup
---------------
Configure the app, leaving wp-config.php *outside* of DocumentRoot:

    $EDITOR wp-config.php
    
    DB Name: dev_sds
    DB User: root
    DB pass: ''
    enable debug: WP_DEBUG > true

Change Upload Permissions
    cd /usr/local/zend/apache2/htdocs/sds/www
    sudo chown -R daemon wp-content/uploads

localhost Setup
---------------

edit Hosts file

    cd ~
    sudo vim /private/etc/hosts

    "i" to go into insert mode

    add 127.0.0.1 sds.local

    "esc" to exit insert mode
    
    :wq to write and quit the file

Add vhost

    cd /usr/local/zend/apache2/conf/extra
    $EDITOR httpd-vhosts.conf

    <VirtualHost *:80>
        DocumentRoot "/usr/local/zend/apache2/htdocs/sds/www"
        ServerName sds.local
    </VirtualHost>

Restart Local Server

    Apache:
    ex. zend restart-apache
    
    MySQL:
    ex. zend restart-mysql

Database Setup
---------------

Add Database Script to local bin
    
    cd ~
    cd bin
    touch sdsdb
    chmod +x sdsdb
    $EDITOR sdsdb

    File contents:

        #!/bin/bash
        cd ~
        cd /usr/local/zend/apache2/htdocs/sds/data
        mysql -u root < sds-drop.sql
        mysql -u root < sds-init.sql
        mysql -u root dev_sds < eicholtz_sds.sql
        mysql -u root dev_sds < sds-update.sql

    Save sdsdb

    run sdsdb

Wordpress Login info
    U: Admin
    P: devsds123

