<?php
/**
 * Single Post Template
 *
 * …
 * 
 * @package Push
 * @subpackage Templates
 */
?>


<?php get_header() ?>

<?php single_before_content_container() ?>

<?php while(have_posts()): the_post() ?>
<section class="template-single">

    <?php single_before_content() ?>
    	    
        <?php single_content() ?>
    
    <?php single_after_content() ?>

</section>
<?php endwhile ?>

<?php single_after_content_container() ?>
   
<?php get_footer() ?>




