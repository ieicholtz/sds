<?php
/**
 * Template Name: Blog
 *
 * Blog Template for Push Projects
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>

<?php get_header() ?>

<?php blog_before_content_container() ?>

<section class="template-blog">

<?php while ( have_posts() ) : the_post() ?>

    <?php if (has_post_thumbnail()): ?>

        <?php
            $domsxe = simplexml_load_string(get_the_post_thumbnail());
            $thumbnailsrc = $domsxe->attributes()->src;
        ?>

        <div id="heading-hero" class="bg-cover" style="background-image: url('<?php echo $thumbnailsrc ?>');"></div>

    <?php endif ?>
    
    <div id="blog-container" class="grid-container">

        <div class="grid-parent grid-25">

            <?php blog_before_get_sidebar() ?>

                <?php blog_get_sidebar() ?>

            <?php blog_after_get_sidebar() ?>
        </div>

        <div class="grid-parent grid-70 prfix-5">

            <?php blog_before_content() ?>

                <?php blog_content() ?>

            <?php blog_after_content() ?>
        </div>

    </div>

<?php endwhile ?>

</section>

<?php blog_after_content_container() ?>

<?php get_footer() ?>


