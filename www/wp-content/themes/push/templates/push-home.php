<?php
/**
 * Template Name: Home
 *
 * Home Page Template for Push Projects
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>

<?php get_header() ?>

<?php home_before_content_container() ?>

<section class="template-home">
<?php while(have_posts()): the_post() ?>

    <?php home_before_slideshow() ?>

        <?php home_slideshow() ?>

    <?php home_after_slideshow() ?>

    <?php home_before_content() ?>

        <?php home_content() ?>

    <?php home_after_content() ?>
   
    <?php home_before_callouts() ?>

        <?php home_callouts() ?>

    <?php home_after_callouts() ?>
<?php endwhile ?>
</section>

<?php home_after_content_container() ?>

<?php get_footer() ?>
