<?php
/**
 * Template Name: Left-Sidebar
 *
 * Left-Sidebar Template for Push Projects
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>

<?php get_header() ?>

<?php left_sidebar_before_content_container() ?>

<section id="template-left-sidebar">

    <?php while(have_posts()): the_post() ?>

        <?php if (has_post_thumbnail()): ?>

            <?php
                $domsxe = simplexml_load_string(get_the_post_thumbnail());
                $thumbnailsrc = $domsxe->attributes()->src;
            ?>

            <div id="heading-hero" class="bg-cover" style="background-image: url('<?php echo $thumbnailsrc ?>');"></div>

        <?php endif ?>

        <div id="template-left-sidebar-content" class="grid-container">
        
            <?php left_sidebar_before_sidebar_container() ?>

            <div id="sidebar" class="grid-parent grid-20">
        
                <?php left_sidebar_before_sidebar() ?>
                    <?php left_sidebar_sidebar() ?>
                <?php left_sidebar_after_sidebar() ?>
            </div>

            <div class="grid-parent grid-75 prefix-5">

                <?php left_sidebar_before_content() ?>
                    <?php left_sidebar_content() ?>
                <?php left_sidebar_after_content() ?>

            </div>
                    
        </div>

    <?php endwhile ?>

</section>

<?php left_sidebar_after_content_container() ?>

<?php get_footer() ?>


