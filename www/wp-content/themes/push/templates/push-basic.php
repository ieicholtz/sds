<?php
/**
 * Template Name: Basic
 *
 * Basic Page Template for Push Projects
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>

<?php get_header() ?>

<?php basic_before_content_container() ?>

<section class="template-basic">

<?php while(have_posts()): the_post() ?>

    <?php if (has_post_thumbnail()): ?>

        <?php
            $domsxe = simplexml_load_string(get_the_post_thumbnail());
            $thumbnailsrc = $domsxe->attributes()->src;
        ?>

        <div id="heading-hero" class="bg-cover" style="background-image: url('<?php echo $thumbnailsrc ?>');"></div>

    <?php endif ?>

    <div class="grid-container">

      <?php basic_before_content() ?>

          <?php basic_content() ?>

      <?php basic_after_content() ?>

    </div>
<?php endwhile ?>

</section>

<?php basic_after_content_container() ?>


<?php get_footer() ?>
