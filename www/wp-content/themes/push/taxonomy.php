<?php
/**
 * Taxonomy Template
 *
 * -
 *
 * @package Push
 * @subpackage Page
 *
 */
?>

<?php get_header() ?>

<?php taxonomy_before_content_container() ?>

<section class="template-taxonomy">

    <?php taxonomy_before_content() ?>

        <?php taxonomy_content() ?>        

    <?php taxonomy_after_content() ?>

</section>

<?php taxonomy_after_content_container() ?>

<?php get_footer() ?>
