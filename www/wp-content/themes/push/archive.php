<?php
/**
 * Archive Template 
 *
 * Displays an Archive index of post-type items. Other more specific archive templates 
 * may override the display of this template for example the category.php.
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>

<?php get_header() ?>

<?php archive_before_content_container() ?>

<section class="template-archive">

    <?php archive_before_content() ?>

        <?php archive_content() ?>

    <?php archive_after_content() ?>

</section>

<?php archive_after_content_container() ?>

<?php get_footer() ?>
