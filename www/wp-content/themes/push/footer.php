<?php
/**
 * Footer Template
 *
 * This template closes #main div and displays the #footer div.
 * 
 * 
 * @package Push
 * @subpackage Templates
 */
?>

<?php push_before_footer() ?>

<footer>

	<?php push_before_footer_content() ?>

        <?php push_footer_content() ?>

	<?php push_after_footer_content() ?>

</footer>

<?php push_after_footer() ?>

<?php push_default_footer_scripts() ?>

<?php push_footer_scripts() ?>

<?php wp_footer() ?>
	
</body>
</html>
