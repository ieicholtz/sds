<?php
/**
 * Shortcodes
 *
 * @package library
 * @subpackage Shortcodes
 * 
 * List of reusable shortcodes
 * Included: 
 *
 * [fulldate start="" end=""]
 * [monthDay start="" end=""]
 * [shortmonth start=""]
 * [day start=""]
 *
 *
 */


// [fulldate start="value" end=""]
function fulldate_func( $atts ) {
    extract( shortcode_atts( array(
        'start' => '',
        'end' => '',
    ), $atts ) );
  
    $startDates = explode(" ", $atts['start']);
    if($atts['end'] != ''):
        $endDates = explode(" ", $atts['end']);
        return $startDates[0].' '.$startDates[1].' - '.$endDates[0].' '.$endDates[1].' '.$endDates[2];
    else:
        return $startDates[0].' '.$startDates[1].' '.$startDates[2];
    endif;

}
add_shortcode( 'fulldate', 'fulldate_func' );

// [monthDay start="value" end=""]
function monthday_func( $atts ) {
    extract( shortcode_atts( array(
        'start' => '',
        'end' => '',
    ), $atts ) );
  
    $startDates = explode(" ", $atts['start']);
    if($atts['end'] != ''):
        $endDates = explode(" ", $atts['end']);
        return $startDates[0].' '.$startDates[1].' - '.$endDates[0].' '.$endDates[1];
    else:
        return $startDates[0].' '.$startDates[1];
    endif;

}
add_shortcode( 'monthday', 'monthday_func' );



// [shortmonth start="value"]
function shortmonth_func( $atts ) {
    extract( shortcode_atts( array(
        'start' => '',
    ), $atts ) );
  
    $startDates = explode(" ", $atts['start']);
    $month = substr($startDates[0], 0,3);

    return strtoupper($month);

}
add_shortcode( 'shortmonth', 'shortmonth_func' );


// [day start="value"]
function day_func( $atts ) {
    extract( shortcode_atts( array(
        'start' => '',
    ), $atts ) );
  
    $startDates = explode(" ", $atts['start']);
  
    return $startDates[1];

}
add_shortcode( 'day', 'day_func' );













