<?php
/**
 * Excerpt Functions
 *
 * @package library
 * @subpackage Excerpts
 * 
 * List of reusable functions for modifying excerpts
 * Included: 
 *
 * push_auto_excerpt()
 *
 *
 */



/**
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis 
 *
 * To override this in a child theme, remove the filter and add your own
 * function tied to the excerpt_more filter hook.
 *
 * @since Push 1.0
 * @return string An ellipsis
 */
function push_auto_excerpt( $more ) {
    return ' &hellip;';
}
add_filter( 'excerpt_more', 'push_auto_excerpt' );



/**
 * trim_excerpt()
 * @since push 1.1
 * @return modified excerpt
 */
function trim_excerpt($excerpt, $width = 75, $suffix = '<span class="ellipsis">&hellip;</span>')
{
    $lines = explode("\n", wordwrap($excerpt, $width));
    return $lines[0].$suffix;
}
