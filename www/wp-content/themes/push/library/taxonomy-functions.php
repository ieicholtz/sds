<?php
/**
 * Taxonomy Functions
 *
 * @package library
 * @subpackage Taxonomies
 * 
 * List of reusable functions for sorting cats and taxonmies
 * Included: 
 *
 * register_taxonmies()
 * get_custom_tax_name($post->ID, $taxonomy)
 * get_custom_tax_slug($post->ID, $taxonomy)
 * get_custom_tax_link($post->ID, $taxonomy)
 *
 *
 */


/**
 *
 * Push Custom Taxonomies
 *
 * @since Push 1.0
 * @uses register_taxonomy
 *
 * Uncomment to include
 *
 * Included:
 *
 * blog_type
 * countries
 * induction_categories
 * 
 *
 */
function build_taxonomies() {
    register_taxonomy( 'service_type', 'service', array( 'hierarchical' => true, 'label' => 'Service Type', 'query_var' => true, 'rewrite' => true ) );
    //register_taxonomy( 'countries', 'post', array( 'hierarchical' => true, 'label' => 'Country', 'query_var' => true, 'rewrite' => true ) );
    //register_taxonomy( 'induction_categories', 'post', array( 'hierarchical' => true, 'label' => 'Induction Category', 'query_var' => true, 'rewrite' => true ) );  
    //register_taxonomy( 'state', 'location', array( 'hierarchical' => true, 'label' => 'State', 'query_var' => true, 'rewrite' => true ) );
    //register_taxonomy( 'section', 'menus', array( 'hierarchical' => true, 'label' => 'Section', 'query_var' => true, 'rewrite' => true ) );
    //register_taxonomy( 'catering-section', 'catering-menu', array( 'hierarchical' => true, 'label' => 'Catering Section', 'query_var' => true, 'rewrite' => true ) );
}


/**
 *
 * Get Custom Taxonomy Name
 *
 * @since Push 1.0
 * @uses $post->ID, $taxonomy
 * @return tax->name
 *
 */
function get_custom_tax_name( $ID, $tax ){
	$catName = '';
	$cats = wp_get_object_terms( $ID, $tax );
  	foreach($cats as $cat){
  		$catName = $cat->name;
  	}
  	        	    
	return $catName;
}

/**
 *
 * Get Custom Taxonomy Slug
 *
 * @since Push 1.0
 * @uses $post->ID, $taxonomy
 * @return tax->slug
 *
 */
function get_custom_tax_slug( $ID, $tax ){
	$catSlug = '';
	$cats = wp_get_object_terms( $ID, $tax );
  	foreach($cats as $cat){
  		$catSlug = $cat->slug;
  	}
  	        	    
	return $catSlug;
}

/**
 *
 * Get Custom Taxonomy Link
 *
 * @since Push 1.0
 * @uses get_custom_tax_slug()
 * @uses $post->ID, $taxonomy
 * @return tax->slug
 *
 */
function get_custom_tax_link( $ID, $tax ){
	$slug = '';
	$catLink = '';
	$slug = get_custom_tax_slug($ID, $tax);
	$catLink = get_term_link($slug, $tax);
	     	    
	return $catLink;
}



/**
 *
 * Get Custom Taxonomy Description
 *
 * @since Push 1.0
 * @uses $post->ID, $taxonomy
 * @return tax->description
 *
 */
function get_custom_tax_desc( $ID, $tax ){
	$catDesc = '';
	$cats = wp_get_object_terms( $ID, $tax );
  	foreach($cats as $cat){
  		$catDesc = $cat->description;
  	}
  	        	    
	return $catDesc;
}
