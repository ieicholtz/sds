<?php
/**
 * Custom Functions
 *
 * @package library
 * @subpackage Function
 * 
 * List of misc reusable functions
 * Included:
 * 
 * generate_custom_nav()
 * breadcrumbs()
 * loginArgs()
 *
 */

function generate_custom_nav($postType){
    $url = '';
    if($postType == 'event'):
        $url = '/visit-the-museum/events/';
    elseif($postType == 'news'):
        $url = '/hall-of-fame/member-news/';
    endif;

    $years = array();
    $items = array();
    $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
    $args = array( 'post_type' => $postType, 'posts_per_page' => '-1', 'order' => 'DESC' );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
        $start = get_field('start_date');
        $startDates = explode(" ", $start);

        $data = array(
            'year'=>$startDates[2],
            'month'=>$startDates[0],
        );

        array_push($items, $data);
        $data = array();

        if(!in_array($startDates[2], $years)):
            array_push($years, $startDates[2]);
        endif;
        
    endwhile;
    wp_reset_query();
    endif;

    $content = '';
    $content = '<ul id="'.$postType.'-subnav" class="submenu">';
    foreach($years as $year):
        $content .= '<li id="'.$year.'">';
        $content .= '<a href="'.$url.'?y='.$year.'">'.$year.'</a>';
        $content .= '</li>';
            $content .= '<ul id="'.$year.'-subnav" class="submenu">';
                foreach($months as $month):
                    $hasMonth = false;
                    foreach($items as $item):
                        if($item['year'] == $year && $item['month'] == $month):
                            $hasMonth = true;
                        endif;
                    endforeach;
                        if($hasMonth):
                            $content .= '<li id="'.$month.'-'.$year.'">';
                            $content .= '<a href="'.$url.'?y='.$year.'&m='.$month.'">'.$month.'</a>';
                            $content .= '</li>';
                        endif;
                endforeach;
            $content .= '</ul>';
        
    endforeach;
    $content .= '</ul>';

    echo $content;
}








function breadcrumbs() {

    global $post;

    $crumbs = '';
    $delimiter = '<li><span class="arrow">&gt;</span></li>';
    $showCurrent = 1;
    $crumbs .= '<ul class="crumbs grid-50">';
        $crumbs .= '<li>';
            $crumbs .= '<a href="'.get_option('home').'" alt="home">HOME</a>';
        $crumbs .= '</li>';

        $crumbs .= $delimiter;

        if(is_page() && !$post->post_parent){

            $crumbs .= '<li class="current">';
                $crumbs .= strtoupper(get_the_title());
            $crumbs .= '</li>';
        
        }elseif(is_page() && $post->post_parent){
            $parentID  = $post->post_parent; 
            $breadcrumbs = array();  
            while ($parentID) {  
                $page = get_page($parentID);  
                $breadcrumbs[] = array('link' => get_permalink($page->ID), 'title' => get_the_title($page->ID));  
                $parentID  = $page->post_parent;  
            }  
            $breadcrumbs = array_reverse($breadcrumbs);  
            for ($i = 0; $i < count($breadcrumbs); $i++) {
                $crumbs .= '<li>';  
                $crumbs .= '<a href="'.$breadcrumbs[$i]['link'].'" alt="'.$breadcrumbs[$i]['title'].'">';
                $crumbs .= strtoupper($breadcrumbs[$i]['title']);
                $crumbs .= '</a></li>';
                if ($i != count($breadcrumbs)-1){
                    $crumbs .= $delimiter;
                }   
            }
            if(isset($_GET['y']) || isset($_GET['m'])){
                $yr = '';
                $mon = '';
                $yr = $_GET['y'];
                if(isset($_GET['m'])){
                    $mon = $_GET['m'];
                }
                if($yr != '' && $mon != ''){
                    $crumbs .= $delimiter;
                    $crumbs .= '<li>';
                        $crumbs .= '<a href="'.get_permalink().'" alt="'.get_the_title().'">';
                            $crumbs .= strtoupper(get_the_title());
                        $crumbs .= '</a>';
                    $crumbs .= '</li>';

                    $crumbs .= $delimiter;
                    $crumbs .= '<li>';
                        $crumbs .= '<a href="'.get_permalink().'?y='.$yr.'" alt="'.$yr.'">';
                            $crumbs .= $yr;
                        $crumbs .= '</a>';
                    $crumbs .= '</li>';

                    $crumbs .= $delimiter;
                    $crumbs .= '<li class="current">';
                        $crumbs .= strtoupper($mon);
                    $crumbs .= '</li>';
                }else{
                    $crumbs .= $delimiter;
                    $crumbs .= '<li>';
                        $crumbs .= '<a href="'.get_permalink().'" alt="'.get_the_title().'">';
                            $crumbs .= strtoupper(get_the_title());
                        $crumbs .= '</a>';
                    $crumbs .= '</li>';

                    $crumbs .= $delimiter;
                    $crumbs .= '<li class="current">';
                        $crumbs .= $yr;
                    $crumbs .= '</li>';
                }

            }else{
                if ($showCurrent == 1){
                
                $crumbs .= $delimiter;
                $crumbs .= '<li class="current">';
                    $crumbs .= strtoupper(get_the_title());
                $crumbs .= '</li>';
                } 
            } 
            

        }elseif(is_single()){
            $type = get_post_type( $post );

            if($type == 'event'){
    
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/visit-the-museum" alt="visit-the-museum">';
                        $crumbs .= 'VISIT THE MUSEUM';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

                $crumbs .= $delimiter;
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/visit-the-museum/events" alt="events">';
                        $crumbs .= 'EVENTS';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

            }
            if($type == 'news'){

                $crumbs .= '<li>';
                    $crumbs .= '<a href="/hall-of-fame" alt="hall-of-fame">';
                        $crumbs .= 'HALL OF FAME';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

                $crumbs .= $delimiter;
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/hall-of-fame/member-news" alt="member-news">';
                        $crumbs .= 'MEMBER NEWS';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';
            }

            if($type == 'inductions'){

                $crumbs .= '<li>';
                    $crumbs .= '<a href="/induction" alt="induction">';
                        $crumbs .= 'INDUCTION';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

                $crumbs .= $delimiter;
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/induction/induction-history" alt="induction-history">';
                        $crumbs .= 'INDUCTION HISTORY';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';
            }
            
            if($type == 'press_releases'){

                $crumbs .= '<li>';
                    $crumbs .= '<a href="/media-center" alt="media center">';
                        $crumbs .= 'MEDIA CENTER';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

            }

            if($type == 'media_galleries'){
               
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/media-center" alt="media center">';
                        $crumbs .= 'MEDIA CENTER';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

            }

            if($type == 'post'){
            
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/hall-of-fame" alt="hall of fame">';
                        $crumbs .= 'HALL OF FAME';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';

                $crumbs .= $delimiter;
                $crumbs .= '<li>';
                    $crumbs .= '<a href="/hall-of-fame/search-hall-of-fame-members" alt="member search">';
                        $crumbs .= 'SEARCH MEMBERS';
                    $crumbs .= '</a>';
                $crumbs .= '</li>';
            
            }


            $crumbs .= $delimiter;
            $crumbs .= '<li class="current">';
                $crumbs .= strtoupper(get_the_title());
            $crumbs .= '</li>';
        }

    $crumbs .= '</ul>';

    echo $crumbs;
       
}




function loginArgs() {

    $args = array( 
      'redirect' => site_url( $_SERVER['REQUEST_URI'] ),  
      'form_id' => 'loginform', 
      'label_username' => '', 
      'label_password' => '', 
      'label_log_in' => __( 'Submit' ), 
      'id_username' => 'user_login',
      'id_password' => 'user_pass',
      'id_remember' => 'rememberme',
      'id_submit' => 'wp-submit',
      'remember' => false,
      'value_username' => NULL,
      'value_remember' => false 
      );

    return $args;
}





function parseName($name) {
    
    $fullName = array(); 
    $splitName = explode(',', $name);
    $fullName['first'] = $splitName[1];
    $fullName['last'] = $splitName[0];
    
    return $fullName;
}




function formatVideo($video) {

    $videoTrim = rtrim($video, '&feature=youtu.be');
    $videoEmbed = str_replace('watch?v=', 'embed/', $videoTrim); 

    return $videoEmbed;

}


function generateCatsFromSubPages($parentId, $category = 'category') {
    $defaults = array(
		'depth' => 0, 
        'show_date' => '',
		'date_format' => get_option('date_format'),
		'child_of' => $parentId, 
        'exclude' => '',
		'title_li' => __('Pages'), 
        'echo' => 1,
		'authors' => '', 
        'sort_column' => 'menu_order, post_title',
		'link_before' => '', 
        'link_after' => '', 
        'walker' => '',
	);

	$r = wp_parse_args( $defaults );
	extract( $r, EXTR_SKIP );

	$output = '';
	$current_page = 0;

	// sanitize, mostly to keep spaces out
	$r['exclude'] = preg_replace('/[^0-9,]/', '', $r['exclude']);

	// Allow plugins to filter an array of excluded pages (but don't put a nullstring into the array)
	$exclude_array = ( $r['exclude'] ) ? explode(',', $r['exclude']) : array();
	$r['exclude'] = implode( ',', apply_filters('wp_list_pages_excludes', $exclude_array) );

	// Query pages.
	$r['hierarchical'] = 0;
	$pages = get_pages($r);

	if ( !empty($pages) ) {
		if ( $r['title_li'] )
			$output .= '<ul>';

		global $wp_query;
		if ( is_page() || is_attachment() || $wp_query->is_posts_page )
			$current_page = $wp_query->get_queried_object_id();
		foreach($pages as $singlePage) {
			$pTitle = $singlePage->post_title;
			$pName = $singlePage->post_name;

			$term = term_exists($pTitle, $category);

			if(!$term){
				wp_insert_term($pTitle, $category, array('slug'=>$pName.'-tax'));
			}
			
		}
	} 
}



function generate_postType_nav($args, $catSlug, $postSlug, $tax='category', $parentID=''){

    $postsList = array();
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
        global $post;
        $catLink = get_permalink();
        if ($parentID) {
            $catLink = get_permalink().'?parent='.$parentID;
        }
        $data = array(
            'title'=>get_the_title(),
            'link'=>$catLink,
            'slug'=>$post->post_name,
            'cat'=>get_custom_tax_slug(get_the_ID(), $tax),
        );

        array_push($postsList, $data);
        $data = array();

        
    endwhile;
    wp_reset_query();
    endif;

    $content = '';
    $content = '<ul id="'.$catSlug.'-subnav" class="menu">';
    foreach($postsList as $pList):
        if ($pList['cat'] == $catSlug):
            if ($pList['slug'] == $postSlug):
                $content .= '<li id="'.$pList['slug'].'" class="current-menu-item">';
            else:
                $content .= '<li id="'.$pList['slug'].'">';
            endif;
            $content .= '<a href="'.$pList['link'].'">'.$pList['title'].'</a>';
            $content .= '</li>';
        endif;
    endforeach;
    $content .= '</ul>';

    echo $content;
}


