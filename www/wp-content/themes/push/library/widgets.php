<?php
/**
 * Custom Widgets
 *
 * @package library
 * @subpackage Widgets
 * 
 * uncomment register_widget to include widget
 * @uses unregister_widget()
 * @uses register_widget()
 * @uses register_sidebar()
 * 
 * Included: 
 * - Social Widget
 *
 */

//Load Widgets
function load_widgets() {



	/* ----- Unregister Widgets ----- */



	// Remove WP default Widgets
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Search' );
    unregister_widget( 'WP_Widget_Pages' );
    //unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Calendar' );
    //unregister_widget( 'WP_Widget_Categories' );
    unregister_widget( 'WP_Nav_Menu_Widget' );
    unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_Text' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );


    
    /* ----- Register Widgets ----- */



    /**
     * Social Widget
     */ 
    class Social_Widget extends WP_Widget {  
        
        function Social_Widget() {  
            
            parent::WP_Widget(false, 'Social Menu');  
        
        }  
        
        function form($instance) { ?>
            <?php $instance = wp_parse_args( (array) $instance, array( 'twitter' => '', 'facebook' => '', 'linkedin' => '', 'youtube' => '' ) ) ?>
                
            <p>
              <label for="<?php echo $this->get_field_id( 'twitter' ); ?>"><?php _e('Twitter Url:'); ?></label>
              <input id="<?php echo $this->get_field_id( 'twitter' ); ?>" name="<?php echo $this->get_field_name( 'twitter' ); ?>" value="<?php echo $instance['twitter']; ?>"/>
            </p>
        
            <p>
              <label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e('Facebook Url:'); ?></label>
              <input id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" value="<?php echo $instance['facebook']; ?>"/>
            </p>
        
            <p>
              <label for="<?php echo $this->get_field_id( 'linkedin' ); ?>"><?php _e('LinkedIn Url:'); ?></label>
              <input id="<?php echo $this->get_field_id( 'linkedin' ); ?>" name="<?php echo $this->get_field_name( 'linkedin' ); ?>" value="<?php echo $instance['linkedin']; ?>"/>
            </p>
        
            <p>
              <label for="<?php echo $this->get_field_id( 'youtube' ); ?>"><?php _e('Youtube Url:'); ?></label>
              <input id="<?php echo $this->get_field_id( 'youtube' ); ?>" name="<?php echo $this->get_field_name( 'youtube' ); ?>" value="<?php echo $instance['youtube']; ?>"/>
            </p>     
        
        <?php }  
        
        function update($new_instance, $old_instance) {
            $instance = $old_instance;
            $instance['twitter'] =  $new_instance['twitter'];
            $instance['facebook'] =  $new_instance['facebook'];
            $instance['linkedin'] =  $new_instance['linkedin'];
            $instance['youtube'] =  $new_instance['youtube'];  
            
            // processes widget options to be saved  
            return $instance;  
        }  
        
        function widget($args, $instance) {  ?>
            
            <ul>    
                <li id='tw' class='textIndent'><a href='<?php echo $instance['twitter']; ?>' target='_blank'>Twitter</a></li>
                <li id='fb' class='textIndent'><a href='<?php echo $instance['facebook']; ?>' target='_blank'>Facebook</a></li>
                <li id='li' class='textIndent'><a href='<?php echo $instance['linkedin']; ?>' target='_blank'>LinkedIn</a></li>
                <li id='yt' class='textIndent'><a href='<?php echo $instance['youtube']; ?>' target='_blank'>YouTube</a></li>
            </ul>

        <?php }  
        
    } //end Social Widget  
        
    //register_widget('Social_Widget');



    /* ----- Register Sidebars ----- */



    /**
     * Footer Social nav
     */ 
  	/*register_sidebar( array(
      'name' => __( 'Footer Social Nav', 'push' ),
      'id' => 'footer-social-nav',
      'description' => __( 'Footer social navigation area', 'push' ),
    ) );*/

    /**
     * Blog Sidebar Widget
     */ 
    register_sidebar( array(
      'name' => __( 'Blog Sidebar Widget', 'push' ),
      'id' => 'blog-sidebar-widget',
      'description' => __( 'Blog Sidebar Widget', 'push' ),
    ) );

    /**
     * Twitter Widget
     */
    /*register_sidebar( array(
        'name' => __('Twitter Widget', 'push'),
        'id' => 'twitter-widget',
        'description' => __('Twitter Feed Widget', 'push'),
    ));*/


} // end load_widgets
?>
