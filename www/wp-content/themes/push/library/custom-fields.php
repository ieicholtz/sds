<?php
/**
 * Custom Fields
 *
 * @package library
 * @subpackage ACF
 * 
 * List of default reusable custom fields
 * Included: 
 *
 * my_acf_settings()
 *
 */

function my_acf_options_page_settings( $settings )
{
    $settings['title'] = 'Global';
    $settings['pages'] = array('Header', 'Footer', '404');

    return $settings;
}

add_filter('acf/options_page/settings', 'my_acf_options_page_settings');


