<?php
/**
 * Post Types
 *
 * @package library
 * @subpackage Post Types
 * 
 * uncomment the register_post_type for any type to be added
 * Included: 
 * - Case Studies 
 * - Partners 
 * - Managers  
 * - Directors 
 * - Jobs
 * - Articles 
 * - Advisors 
 * - Press Releases
 * - Events
 * - News
 * - Inductions
 * - Media Galleries
 * - Locations 
 * - Locations 
 * - Menu
 * - Catering Menu
 * - Products & Services
 * - Team
 */

//Load Post Types
function load_post_types() {

	// Case Studies
	$labels = array(
	    'name'          => _x( 'Case Studies', 'post type general name' ),
	    'singular_name' => _x( 'Case Study', 'post type singular name' ),
	    'add_new' 		=> _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Case Study' ),
	    'edit_item'     => __( 'Edit Case Study' ),
	    'new_item'      => __( 'New Case Study' ),
	    'all_items'     => __( 'All Case Studies' ),
	    'view_item'     => __( 'View Case Studies' ),
	    'search_items'  => __( 'Search Case Studies' ),
	    'not_found'     => __( 'No case studies found' ),
	    'not_found_in_trash' => __( 'No case studies found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Case Studies'
	);
	
	 $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Case Study Item Data',
	    'public'        => true,
	    'menu_position' => 5,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'case study', $args );
	
	
	// Partners
	$labels = array(
	    'name'          => _x( 'Partners', 'post type general name' ),
	    'singular_name' => _x( 'Partner', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Partner' ),
	    'edit_item'     => __( 'Edit Partner' ),
	    'new_item'      => __( 'New Partner' ),
	    'all_items'     => __( 'All Partners' ),
	    'view_item'     => __( 'View Partners' ),
	    'search_items'  => __( 'Search Partners' ),
	    'not_found'     => __( 'No partners found' ),
	    'not_found_in_trash' => __( 'No case studies found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Partners'
	);
	
	$args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Partner Item Data',
	    'public'        => true,
	    'menu_position' => 5,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'taxonomies'    => array('custom'),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'partners', $args );
	
	
	
	// Managers	
	$labels = array(
	    'name'          => _x( 'Managers', 'post type general name' ),
	    'singular_name' => _x( 'Manager', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Manager' ),
	    'edit_item'     => __( 'Edit Manager' ),
	    'new_item'      => __( 'New Manager' ),
	    'all_items'     => __( 'All Managers' ),
	    'view_item'     => __( 'View Managers' ),
	    'search_items'  => __( 'Search Managers' ),
	    'not_found'     => __( 'No managers found' ),
	    'not_found_in_trash' => __( 'No managers found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Managers'
	);
	
	$args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Manager Item Data',
	    'public'        => true,
	    'menu_position' => 5,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'managers', $args );
	
	
	
	
	// Directors
	$labels = array(
	    'name'          => _x( 'Directors', 'post type general name' ),
	    'singular_name' => _x( 'Director', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Director' ),
	    'edit_item'     => __( 'Edit Director' ),
	    'new_item'      => __( 'New Director' ),
	    'all_items'     => __( 'All Directors' ),
	    'view_item'     => __( 'View Directors' ),
	    'search_items'  => __( 'Search Directors' ),
	    'not_found'     => __( 'No directors found' ),
	    'not_found_in_trash' => __( 'No directors found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Directors'
	);
	
	$args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Director Item Data',
	    'public'        => true,
	    'menu_position' => 5,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'directors', $args );
	
	
	
	// Advisors
	$labels = array(
	    'name'          => _x( 'Advisors', 'post type general name' ),
	    'singular_name' => _x( 'Advisor', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Advisor' ),
	    'edit_item'     => __( 'Edit Advisors' ),
	    'new_item'      => __( 'New Advisor' ),
	    'all_items'     => __( 'All Advisors' ),
	    'view_item'     => __( 'View Advisors' ),
	    'search_items'  => __( 'Search Advisors' ),
	    'not_found'     => __( 'No advisors found' ),
	    'not_found_in_trash' => __( 'No advisors found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Advisors'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Advisor Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
		
	//register_post_type( 'advisors', $args );
	
	
	// Jobs
	$labels = array(
	    'name'          => _x( 'Jobs', 'post type general name' ),
	    'singular_name' => _x( 'Job', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Job' ),
	    'edit_item'     => __( 'Edit Jobs' ),
	    'new_item'      => __( 'New Job' ),
	    'all_items'     => __( 'All Jobs' ),
	    'view_item'     => __( 'View Jobs' ),
	    'search_items'  => __( 'Search Jobs' ),
	    'not_found'     => __( 'No jobs found' ),
	    'not_found_in_trash' => __( 'No jobs found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Jobs'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Job Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'jobs', $args );
	

	// Articles
	$labels = array(
	    'name'          => _x( 'Articles', 'post type general name' ),
	    'singular_name' => _x( 'Article', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Article' ),
	    'edit_item'     => __( 'Edit Articles' ),
	    'new_item'      => __( 'New Article' ),
	    'all_items'     => __( 'All Articles' ),
	    'view_item'     => __( 'View Articles' ),
	    'search_items'  => __( 'Search Articles' ),
	    'not_found'     => __( 'No articles found' ),
	    'not_found_in_trash' => __( 'No articles found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Articles'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Article Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'articles', $args );
	
	
	
	// Press Releases
	$labels = array(
	    'name'          => _x( 'Press Releases', 'post type general name' ),
	    'singular_name' => _x( 'Press Release', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Press Release' ),
	    'edit_item'     => __( 'Edit Press Release' ),
	    'new_item'      => __( 'New Press Release' ),
	    'all_items'     => __( 'All Press Releases' ),
	    'view_item'     => __( 'View Press Releases' ),
	    'search_items'  => __( 'Search Press Releases' ),
	    'not_found'     => __( 'No press releases found' ),
	    'not_found_in_trash' => __( 'No press releases found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Press Releases'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Press Release Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'press_releases', $args );

	
	// Events
	$labels = array(
	    'name'          => _x( 'Events', 'post type general name' ),
	    'singular_name' => _x( 'Event', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Event' ),
	    'edit_item'     => __( 'Edit Event' ),
	    'new_item'      => __( 'New Event' ),
	    'all_items'     => __( 'All Events' ),
	    'view_item'     => __( 'View Events' ),
	    'search_items'  => __( 'Search Events' ),
	    'not_found'     => __( 'No events found' ),
	    'not_found_in_trash' => __( 'No events found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Events'
	);
	
	$args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Event Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'event', $args );


	// News
	$labels = array(
	    'name'          => _x( 'News', 'post type general name' ),
	    'singular_name' => _x( 'News', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add News Story' ),
	    'edit_item'     => __( 'Edit News Story' ),
	    'new_item'      => __( 'New News Story' ),
	    'all_items'     => __( 'All News Stories' ),
	    'view_item'     => __( 'View News Stories' ),
	    'search_items'  => __( 'Search News Stories' ),
	    'not_found'     => __( 'No news stories found' ),
	    'not_found_in_trash' => __( 'No news stories found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'News'
	);
	
	$args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual News Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'news', $args );


    // Inductions
	$labels = array(
	    'name'          => _x( 'Inductions', 'post type general name' ),
	    'singular_name' => _x( 'Induction', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Induction' ),
	    'edit_item'     => __( 'Edit Induction' ),
	    'new_item'      => __( 'New Induction' ),
	    'all_items'     => __( 'All Inductions' ),
	    'view_item'     => __( 'View Inductions' ),
	    'search_items'  => __( 'Search Inductions' ),
	    'not_found'     => __( 'No inductions found' ),
	    'not_found_in_trash' => __( 'No inductions found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Inductions'
	);
	
	$args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Induction Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'inductions', $args );
	
    
    // Media Galleries
	$labels = array(
	    'name'          => _x( 'Media Galleries', 'post type general name' ),
	    'singular_name' => _x( 'Media Gallery', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Media Gallery' ),
	    'edit_item'     => __( 'Edit Media Gallery' ),
	    'new_item'      => __( 'New Media Gallery' ),
	    'all_items'     => __( 'All Media Galleries' ),
	    'view_item'     => __( 'View Media Gallery' ),
	    'search_items'  => __( 'Search Media Galleries' ),
	    'not_found'     => __( 'No media galleries found' ),
	    'not_found_in_trash' => __( 'No media galleries found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Media Galleries'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Media Gallery Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'media_galleries', $args );

    // Locations
	$labels = array(
	    'name'          => _x( 'Locations', 'post type general name' ),
	    'singular_name' => _x( 'Location', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Location' ),
	    'edit_item'     => __( 'Edit Location' ),
	    'new_item'      => __( 'New Location' ),
	    'all_items'     => __( 'All Locations' ),
	    'view_item'     => __( 'View Location' ),
	    'search_items'  => __( 'Search Locations' ),
	    'not_found'     => __( 'No locations found' ),
	    'not_found_in_trash' => __( 'No locations found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Locations'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Location Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
    //register_post_type( 'location', $args );


    // Menu
	$labels = array(
	    'name'          => _x( 'Menu Items', 'post type general name' ),
	    'singular_name' => _x( 'Menu Item', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Menu Item' ),
	    'edit_item'     => __( 'Edit Menu Item' ),
	    'new_item'      => __( 'New Menu Item' ),
	    'all_items'     => __( 'All Menu Items' ),
	    'view_item'     => __( 'View Menu Items' ),
	    'search_items'  => __( 'Search Menu Items' ),
	    'not_found'     => __( 'No menu items found' ),
	    'not_found_in_trash' => __( 'No menu items found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Menu'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Menu Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'menus', $args );


    //Catering Menu
	$labels = array(
	    'name'          => _x( 'Catering Menu Items', 'post type general name' ),
	    'singular_name' => _x( 'Catering Menu Item', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Catering Menu Item' ),
	    'edit_item'     => __( 'Edit Catering Menu Item' ),
	    'new_item'      => __( 'New Catering Menu Item' ),
	    'all_items'     => __( 'All Catering Menu Items' ),
	    'view_item'     => __( 'View Catering Menu Items' ),
	    'search_items'  => __( 'Search Catering Menu Items' ),
	    'not_found'     => __( 'No catering menu items found' ),
	    'not_found_in_trash' => __( 'No catering menu items found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Catering Menu'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Catering Menu Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	//register_post_type( 'catering-menu', $args );


    //Services
	$labels = array(
	    'name'          => _x( 'Service Items', 'post type general name' ),
	    'singular_name' => _x( 'Services Item', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Services Item' ),
	    'edit_item'     => __( 'Edit Services Item' ),
	    'new_item'      => __( 'New Services Item' ),
	    'all_items'     => __( 'All Service Items' ),
	    'view_item'     => __( 'View Service Items' ),
	    'search_items'  => __( 'Search Service Items' ),
	    'not_found'     => __( 'No service items found' ),
	    'not_found_in_trash' => __( 'No service items found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Services'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Services Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
	register_post_type( 'service', $args );

    // Team
	$labels = array(
	    'name'          => _x( 'Team', 'post type general name' ),
	    'singular_name' => _x( 'Team Member', 'post type singular name' ),
	    'add_new'       => _x( 'Add New', 'Item' ),
	    'add_new_item'  => __( 'Add New Team Member' ),
	    'edit_item'     => __( 'Edit Team Member' ),
	    'new_item'      => __( 'New Team Member' ),
	    'all_items'     => __( 'All Team Members' ),
	    'view_item'     => __( 'View Team Member' ),
	    'search_items'  => __( 'Search Team Members' ),
	    'not_found'     => __( 'No team members found' ),
	    'not_found_in_trash' => __( 'No team members found in the Trash' ),
	    'parent_item_colon'  => '',
	    'menu_name'          => 'Team'
	);
	
	  $args = array(
	    'labels'        => $labels,
	    'description'   => 'Individual Team Member Item Data',
	    'public'        => true,
	    'menu_position' => 4,
	    'supports'      => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'cats' ),
	    'has_archive'   => true,
	);
	
    //register_post_type( 'team', $args );


}


?>
