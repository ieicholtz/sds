<?php
/**
 * Footer Defaults
 *
 * @package Extensions
 * @subpackage Footer Defaults
 *
 * Hooks:
 *
 * push_before_footer()
 * push_before_footer_content()
 * push_footer_content()
 * push_after_footer_content()
 * push_after_footer()
 *
 */



/* ----- Hooks ----- */



/**
 * Register action hook: push_before_footer
 * 
 */
function push_before_footer() {
   
    do_action( 'push_before_footer' );

}



/**
 * Register action hook: push_before_footer_content
 * 
 */
function push_before_footer_content() {
    
    do_action( 'push_before_footer_content' );

}



/**
 * Register action hook: push_footer_content
 * 
 * Load the footer markup template here 
 * 
 */
if ( function_exists('childtheme_override_footer_content') )  {
  /**
    * @ignore
    */
    function push_footer_content() {
        
        //use child footer template      
        childtheme_override_footer_content();
    }
} else {
  
  /**
   * Load default footer content
   * 
   * Override: childtheme_override_footer_content
   * 
   */
  function push_footer_content() {
      
     $content = '';
     $contnet .= '<h1>'.bloginfo('name').'<h1>';
     $content .= '<p>Push Default Footer Content</p>';
     echo $content;
  }
}



/**
 * Register action hook: push_after_footer_content
 * 
 */
function push_after_footer_content() {
    do_action( 'push_after_footer_content' );
}



/**
 * Register action hook: push_after_footer
 * 
 */
function push_after_footer() {
    do_action( 'push_after_footer' );
}






