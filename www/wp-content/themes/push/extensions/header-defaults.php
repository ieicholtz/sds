<?php
/**
 * Header Defaults
 *
 * @package Extensions
 * @subpackage header Defaults
 *
 * Filters:
 *
 * push_meta()
 * push_create_robots()
 * push_show_robots()
 * push_facebook_meta()
 * push_wptitle()
 * push_seo()
 * 
 * Hooks:
 *
 * push_doctitle()
 * push_icons()
 * push_before_header()
 * push_before_header_content()
 * push_header_content()
 * push_after_header_content()
 * push_after_header()
 *
 */


/* ----- Filters ----- */



/**
 * Site Specific Meta Filter
 * 
 * Filter: push_meta
 */
function push_meta() {
    $content = '';
    echo apply_filters('push_meta', $content );
}



/**
 * Site Specific Facebook Meta Filter
 * 
 * Filter: push_facebook_meta
 */
function push_facebook_meta() {
    $content = '';
    echo apply_filters('push_facebook_meta', $content );
}



/**
 * Site Specific Additional Meta Filter
 * 
 * Filter: push_additional_meta
 */
function push_additional_meta() {
    $content = '';
    echo apply_filters('push_additional_meta', $content );
}



/**
 * Create the robots meta-tag
 * 
 * This can be switched on or off using push_show_robots
 * Credits: Thematic Theme
 * 
 * Filter: push_create_robots
 */
function push_create_robots() {
        global $paged;
    if ( push_seo() ) {
        if ( ( is_home() && ( $paged < 2 ) ) || is_front_page() || is_single() || is_page() || is_attachment() ) {
        $content = '<meta name="robots" content="index,follow" />';
        } elseif ( is_search() ) {
            $content = '<meta name="robots" content="noindex,nofollow" />';
        } else {  
            $content = '<meta name="robots" content="noindex,follow" />';
        }
        $content .= "\n";
        if ( get_option('blog_public') ) {
            echo apply_filters('push_create_robots', $content);
        }
    }
} 



/**
 * Switch creating the robots meta-tag
 * 
 * Default: ON
 * Credits: Thematic Theme
 * 
 * Filter: push_show_robots
 */
function push_show_robots() {
    $display = TRUE;
    $display = apply_filters('push_show_robots', $display);
    if ( $display ) {
        push_create_robots();
    }
} 



/**
 * Filters wp_title returning the doctitle contents
 * Located in header.php Credits: Tarski Theme
 * 
 * Override: childtheme_override_doctitle
 * Filter: push_doctitle_separator
 * Filter: push_doctitle
 *
 * @since 1.0
 */
function push_wptitle( $wp_doctitle, $separator, $sep_location ) { 
    // return original string if on feed or if a seo plugin is being used
    if ( is_feed() || !push_seo() )
        return $wp_doctitle;
    // otherwise... 
    $site_name = get_bloginfo('name' , 'display');
            
    if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
      $content = single_post_title('', FALSE);
    }
    elseif ( is_front_page() ) { 
      $content = get_bloginfo('description', 'display');
    }
    elseif ( is_page() ) { 
      $content = single_post_title('', FALSE); 
    }
    elseif ( is_search() ) { 
      $content = __('Search Results for:', 'push'); 
      $content .= ' ' . get_search_query();
    }
    elseif ( is_category() ) {
      $content = __('Category Archives:', 'push');
      $content .= ' ' . single_cat_title('', FALSE);;
    }
    elseif ( is_tag() ) { 
      $content = __('Tag Archives:', 'push');
      //$content .= ' ' . thematic_tag_query();
    }
    elseif ( is_404() ) { 
      $content = __('Not Found', 'push'); 
    }
    else { 
      $content = get_bloginfo('description', 'display');
    }
    
    if ( get_query_var('paged') ) {
      $content .= ' ' .$separator. ' ';
      $content .= 'Page';
      $content .= ' ';
      $content .= get_query_var('paged');
    }
    
    if($content) {
      if ( is_front_page() ) {
          $elements = array(
            'site_name' => $site_name,
            'separator' => $separator,
            'content'   => $content
          );
      }
      else {
          $elements = array(
            'content' => $content
          );
      }  
    } else {
      $elements = array(
        'site_name' => $site_name
      );
    }
    
    // Filters should return an array
    $elements = apply_filters('push_doctitle', $elements);
       
    // But if they don't, it won't try to implode
    if( is_array($elements) ) {
        $push_doctitle = implode(' ', $elements);
    } else {
        $push_doctitle = $elements;
    }
    
    return $push_doctitle;
}

add_filter( 'wp_title', 'push_wptitle', 10, 3);



/**
 * Switch SEO functions on or off
 * 
 * Provides compatibility with SEO plugins: All in One SEO Pack, HeadSpace, 
 * Platinum SEO Pack, wpSEO and Yoast SEO. Default: ON
 * Credit: Thematic
 * 
 * Filter: push_seo
 */
function push_seo() {
    if ( class_exists('All_in_One_SEO_Pack') || class_exists('HeadSpace_Plugin') || class_exists('Platinum_SEO_Pack') || class_exists('wpSEO') || defined('WPSEO_VERSION') ) {
        $content = FALSE;
    } else {
        $content = true;
    }
        return apply_filters( 'push_seo', $content );
}



/* ----- Hooks ----- */



/**
 * Generate SEO Friendly title
 * 
 */
if ( function_exists('childtheme_override_doctitle') )  {
    /**
     * @ignore
     */
     function push_doctitle() {
        childtheme_override_doctitle();
    }
} else {
    /**
     * Display the content of the title tag
     * 
     * Override: childtheme_override_doctitle
     * Filter: push_doctitle_separator
     *
     */
    function push_doctitle() {
        $separator = apply_filters('push_doctitle_separator', '|');
        $doctitle = '<title>' . wp_title( $separator, false, 'right' ) . '</title>' . "\n";
        echo $doctitle;
    } // end push_doctitle
}



/**
 * Favicon & Apple touch icons
 * NOTE: favicon.ico should still be placed in the webroot
 */
if ( function_exists('childtheme_override_icons') )  {
    /**
     * @ignore
     */
     function push_icons() {
        childtheme_override_icons();
    }
} else {
    /**
     * Display the favicon & apple touch icons
     * 
     * Override: childtheme_override_icons
     *
     */
    function push_icons() {
        $content = '<link href="'.get_bloginfo( 'template_directory' ).'/images/favicon.png" rel="icon" sizes="32x32" type="image/png">';
        $content .= "\r\n";
        $content .= '<link href="'.get_bloginfo( 'template_directory' ).'/images/apple/apple-touch-icon-114x114-precomposed.png" rel="apple-touch-icon-precomposed" sizes="114x114" type="image/png">';
        $content .= "\r\n";
        $content .= '<link href="'.get_bloginfo( 'template_directory' ).'/images/apple/apple-touch-icon-72x72-precomposed.png" rel="apple-touch-icon-precomposed" sizes="72x72" type="image/png">';
        $content .= "\r\n";
        $content .= '<link href="'.get_bloginfo( 'template_directory' ).'/images/apple/apple-touch-icon-57x57-precomposed.png" rel="apple-touch-icon-precomposed" sizes="57x57" type="image/png">';
        $content .= "\r\n";
        $content .= '<link href="'.get_bloginfo( 'template_directory' ).'/images/apple/apple-touch-icon-precomposed.png" rel="apple-touch-icon-precomposed" type="image/png">';
        echo apply_filters('push_icons', $content );
    } // end push_icons
}



/**
 * Register action hook: push_before_header
 * 
 */
function push_before_header() {
    
   do_action('push_before_header');

}



/**
 * Register action hook: push_before_header_content
 * 
 */
function push_before_header_content() {
    
    do_action( 'push_before_header_content' );

}



/**
 * Register action hook: push_header_content
 * 
 * Load the header markup template here 
 * 
 */
if ( function_exists('childtheme_override_header_content') )  {
  /**
   * @ignore
   */
   function push_header_content() {
      childtheme_override_header_content();
    }
} else {
  /**
   * Load default header content
   * 
   * Override: childtheme_override_header_content
   * 
   */
    function push_header_content() {
        
        $content = '';
        $content .= '<h1>'.bloginfo('name').'</h1>';
        $content .- '<p>Push Default Header Content</p>';
        echo $content;
  }
}



/**
 * Register action hook: push_after_header_content
 * 
 */
function push_after_header_content() {
    
    do_action( 'push_after_header_content' );

}



/**
 * Register action hook: push_after_header
 * 
 */
function push_after_header() {
    
    do_action( 'push_after_header' );

}















