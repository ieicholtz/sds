<?php
/**
 * Style Defaults
 *
 * @package Extensions
 * @subpackage Style Defaults
 *
 * Functions:
 *
 * disable_contact7_styles()
 * push_default_styles()
 *
 * Hooks: 
 *
 * push_ie_styles()
 * push_global_styles()
 *
 * Load All Wordpress Default Template Stylesheets
 * Load Custom Template Stylesheets
 *
 */



/* ----- Functions ----- */



/**
 * Disable Contact Form 7 Default Styles
 * 
 */
function disable_contact7_styles() {
	
	wp_deregister_style( 'contact-form-7' );
	wp_deregister_style( 'contact-form-7-rtl' );

}
add_action( 'wp_print_styles', 'disable_contact7_styles' );



/**
 * Load Theme Default Styles
 * 
 */
function push_default_styles() {
	
	//register styles
    wp_register_style('boilerplate_css', get_bloginfo('template_directory').'/styles/boilerplate.css');
    wp_register_style('normalize_css', get_bloginfo('template_directory').'/styles/normalize.css');
    wp_register_style('unsemantic_css', get_bloginfo('template_directory').'/styles/unsemantic-grid-responsive.css');
    wp_register_style('unsemantic-ie_css', get_bloginfo('template_directory').'/styles/unsemantic-grid-ie.css');
    wp_register_style('push-ui_css', get_bloginfo('template_directory').'/styles/push-ui.css');
    //setup conditionals
	global $wp_styles;
	$wp_styles->add_data('unsemantic-ie_css', 'conditional', 'lt IE 9');
	//enqueue styles
    wp_enqueue_style('boilerplate_css');
    wp_enqueue_style('normalize_css');
    wp_enqueue_style('unsemantic_css');
    wp_enqueue_style('unsemantic-ie_css');
    wp_enqueue_style('push-ui_css');

    if(is_404()){
        
        push_404_styles();
    
    }elseif(is_archive()){
        
        archive_styles();
    
    }elseif(is_attachment()){
        
        attachment_styles();
    
    }elseif(is_author()){
        
        author_styles();
    
    }elseif(is_category()){
        
        category_styles();
    
    }elseif(is_search()){

        search_styles();

    }elseif(is_single()){

        single_styles();

    }elseif(is_tag()){

        tag_styles();
    
    }elseif(is_tax()){

        taxonomy_styles();
    
    }elseif(is_date()){

        date_styles();

    }elseif(is_front_page()){

        home_styles();

    }elseif(is_page_template('index.php')){

        index_styles();

    }elseif(is_page_template('page.php')){
        
        page_styles();
    
    }elseif(is_page_template('templates/push-basic.php')){

        basic_styles();

    }elseif(is_page_template('templates/push-blog.php')){
        
        //sidebar_styles();
        blog_styles();

    }elseif(is_page_template('templates/push-faq.php')){

        faq_styles();

    }elseif(is_page_template('templates/push-left-sidebar.php')){

        left_sidebar_styles();

    }elseif(is_page_template('templates/push-sitemap.php')){

        sitemap_styles();

    }elseif(is_page_template('templates/push-top-navigation.php')){
     
        //sidebar_styles();
        top_navigation_styles();

    } 
}



/* ----- Hooks ----- */



/**
 * Register action hook: push_ie_styles
 *
 * Load IE Specific Default Styles
 * 
 */
function push_ie_styles() {
	 
	 do_action('push_ie_styles');

}



/**
 * Register action hook: push_global_styles
 * 
 * Load all global styles from child theme here 
 * 
 * 
 */
function push_global_styles() {

    do_action('push_global_styles');

}



/* ============== Wordpress Default Templates ============= */



/**
 * Register 404 Template CSS
 * 
 */
function push_404_styles() {
	//register styles
	wp_register_style('push_404_css', get_bloginfo('template_directory').'/styles/push-404.css');
	//enqueue styles
	wp_enqueue_style('push_404_css');
}



/**
 * Register Archive Template CSS
 * 
 */
function archive_styles() {
	//register styles
	wp_register_style('push_archive_css', get_bloginfo('template_directory').'/styles/push-archive.css');
	//enqueue styles
	wp_enqueue_style('push_archive_css');
}



/**
 * Register Attachment Template CSS
 * 
 */
function attachment_styles() {
	//register styles
	wp_register_style('push_attachment_css', get_bloginfo('template_directory').'/styles/push-attachment.css');
	//enqueue styles
	wp_enqueue_style('push_attachment_css');
}



/**
 * Register Author Template CSS
 * 
 */
function author_styles() {
	//register styles
	wp_register_style('push_author_css', get_bloginfo('template_directory').'/styles/push-author.css');
	//enqueue styles
	wp_enqueue_style('push_author_css');
}



/**
 * Register Category Template CSS
 * 
 */
function category_styles() {
	//register styles
	wp_register_style('push_category_css', get_bloginfo('template_directory').'/styles/push-category.css');
	//enqueue styles
	wp_enqueue_style('push_category_css');
}



/**
 * Register Index Template CSS
 * 
 */
function index_styles() {
	//register styles
	wp_register_style('push_index_css', get_bloginfo('template_directory').'/styles/push-index.css');
	//enqueue styles
	wp_enqueue_style('push_index_css');
}



/**
 * Register Page Template CSS
 * 
 */
function page_styles() {
	//register styles
	wp_register_style('push_page_css', get_bloginfo('template_directory').'/styles/push-page.css');
	//enqueue styles
	wp_enqueue_style('push_page_css');
}



/**
 * Register Search Template CSS
 * 
 */
function search_styles() {
	//register styles
	wp_register_style('push_search_css', get_bloginfo('template_directory').'/styles/push-search.css');
	//enqueue styles
	wp_enqueue_style('push_search_css');
}


/**
 * Register Sidebar Template CSS
 * 
 */
function sidebar_styles() {
	//register styles
	wp_register_style('push_sidebar_css', get_bloginfo('template_directory').'/styles/push-sidebar.css');
	//enqueue styles
	wp_enqueue_style('push_sidebar_css');
}



/**
 * Register Single Template CSS
 * 
 */
function single_styles() {
	
	//register styles
	wp_register_style('push_single_css', get_bloginfo('template_directory').'/styles/push-single.css');
	//enqueue styles
	wp_enqueue_style('push_single_css');

}



/**
 * Register Tag Template CSS
 * 
 */
function tag_styles() {
	
	//register styles
	wp_register_style('push_tag_css', get_bloginfo('template_directory').'/styles/push-tag.css');
	//enqueue styles
	wp_enqueue_style('push_tag_css');

}



/**
 * Register Taxonomy Template CSS
 * 
 */
function taxonomy_styles() {
	
	//register styles
	wp_register_style('push_taxonomy_css', get_bloginfo('template_directory').'/styles/push-taxonomy.css');
	//enqueue styles
	wp_enqueue_style('push_taxonomy_css');

}



/**
 * Register Date Template CSS
 * 
 */
function date_styles() {
	
	//register styles
	wp_register_style('push_date_css', get_bloginfo('template_directory').'/styles/push-date.css');
	//enqueue styles
	wp_enqueue_style('push_date_css');

}



/* ============= Custom Templates =============== */




/**
 * Register Home Template CSS
 * 
 */
function home_styles() {
	
	//register styles
	wp_register_style('push_home_css', get_bloginfo('template_directory').'/styles/templates/push-home.css');
	//enqueue styles
	wp_enqueue_style('push_home_css');

}



/**
 * Register Basic Template CSS
 * 
 */
function basic_styles() {
	
	//register styles
	wp_register_style('push_basic_css', get_bloginfo('template_directory').'/styles/templates/push-basic.css');
	//enqueue styles
	wp_enqueue_style('push_basic_css');

}



/**
 * Register Blog Template CSS
 * 
 */
function blog_styles() {
	
	//register styles
	wp_register_style('push_blog_css', get_bloginfo('template_directory').'/styles/templates/push-blog.css');
	//enqueue styles
	wp_enqueue_style('push_blog_css');

}



/**
 * Register Left-Sidebar Template CSS
 * 
 */
function left_sidebar_styles() {
	
	//register styles
	wp_register_style('push_left_sidebar_css', get_bloginfo('template_directory').'/styles/templates/push-left-sidebar.css');
	//enqueue styles
	wp_enqueue_style('push_left_sidebar_css');

}



/**
 * Register Modal Template CSS
 * 
 */
function modal_styles() {
	//register styles
	wp_register_style('push_modal_css', get_bloginfo('template_directory').'/styles/templates/push-modal.css');
	//enqueue styles
	wp_enqueue_style('push_modal_css');
}



 /**
 * Register Sitemap Template CSS
 * 
 */
function sitemap_styles() {
	//register styles
	wp_register_style('push_sitemap_css', get_bloginfo('template_directory').'/styles/templates/push-sitemap.css');
	//enqueue styles
	wp_enqueue_style('push_sitemap_css');
}



 /**
 * Register FAQ Template CSS
 * 
 */
function faq_styles() {
	//register styles
	wp_register_style('push_faq_css', get_bloginfo('template_directory').'/styles/templates/push-faq.css');
	//enqueue styles
	wp_enqueue_style('push_faq_css');
}



/**
 * Register Top Navigation Template CSS
 * 
 */
function top_navigation_styles() {
	//register styles
	wp_register_style('push_top_navigation_css', get_bloginfo('template_directory').'/styles/templates/push-top-navigation.css');
	//enqueue styles
	wp_enqueue_style('push_top_navigation_css');
}







