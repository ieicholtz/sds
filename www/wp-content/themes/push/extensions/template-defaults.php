<?php
/**
 * Template Defaults
 *
 * @package Extensions
 * @subpackage Template Defaults
 *
 *
 */



/* ----------- Home  ------------- */



function home_before_content_container() {
   
    do_action('home_before_content_container');

}



function home_before_content() {
    do_action( 'home_before_content' );
}



if ( function_exists('childtheme_override_home_content') )  {
  /**
   * @ignore
   */
   function home_content() {
      
      childtheme_override_home_content();
    
    }
} else {

    function home_content() {
      
      $content = '';
      echo $content;
  
  }
}



/**
 * Register action hook: home_after_content
 * 
 */
function home_after_content() {
    do_action( 'home_after_content' );
}



/**
 * Register action hook: home_after_content_container
 * 
 */
function home_after_content_container() {
    do_action( 'home_after_content_container' );
}



function home_before_slideshow() {
    do_action( 'home_before_slideshow' );
}



if ( function_exists('childtheme_override_home_slideshow') )  {
  /**
   * @ignore
   */
   function home_slideshow() {
      
      childtheme_override_home_slideshow();
    
    }
} else {
  
    function home_slideshow() {
      
      $content= '';
      $content .= '<h2>Slideshow</h2>';
      $content .= '<p>Push Default Slideshow Content</p>';
      echo $content;
  
  }
}


function home_after_slideshow() {
    do_action( 'home_after_slideshow' );
}




function home_before_callouts() {
    do_action( 'home_before_callouts' );
}



/**
 * Register action hook: home_callouts
 * 
 * Load the callouts markup template here 
 * 
 */
if ( function_exists('childtheme_override_home_callouts') )  {
  /**
   * @ignore
   */
   function home_callouts() {
      
      childtheme_override_home_callouts();
    
    }
} else {
  /**
   * Load default callouts content
   * 
   * Override: childtheme_override_home_callouts
   * 
   */
    function home_callouts() {
      
      $content= '';
      $content .= '<h2>Callouts</h2>';
      $content .= '<p>Push Default Callouts Content</p>';
      echo $content;
  
  }
}



/**
* Register action hook: home_after_callouts
* 
*/
function home_after_callouts() {
    do_action( 'home_after_callouts' );
}











/* ----------- Basic ------------- */


/**
 * Register action hook: basic_before_content_container
 * 
 */
function basic_before_content_container() {
    do_action( 'basic_before_content_container' );
}

/**
 * Register action hook: basic_before_content
 * 
 */
function basic_before_content() {    
    do_action( 'basic_before_content' );
}


/**
 * Register action hook: basic_content
 * 
 */
if ( function_exists('childtheme_override_basic_content') )  {
  /**
   * @ignore
   */
   function basic_content() {      
      childtheme_override_basic_content();
    }
}else{
  /**
   * Load default basic content
   * 
   */
    function basic_content() {  
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push default basic content</p>';
      echo $content;
  }
}


/**
 * Register action hook: basic_after_content
 * 
 */
function basic_after_content() { 
    do_action( 'basic_after_content' );
}


/**
 * Register action hook: basic_after_content_container
 * 
 */
function basic_after_content_container() {
    do_action( 'basic_after_content_container' );
}











/* ----------- Left Sidebar ------------- */


function left_sidebar_before_sidebar_container() {
    do_action( 'left_sidebar_before_sidebar_container' );
}



function left_sidebar_before_sidebar() {
    do_action( 'left_sidebar_before_sidebar' );
}



if ( function_exists('childtheme_override_left_sidebar_sidebar') )  {
  /**
   * @ignore
   */
   function left_sidebar_sidebar() {
      
      childtheme_override_left_sidebar_sidebar();
    
    }
} else {
  /**
   * Load default left sidebar get sidebar
   * 
   * Override: childtheme_override_left_sidebar_get_sidebar
   * 
   */
    function left_sidebar_sidebar() {
      
      get_sidebar();
  
  }
}



function left_sidebar_after_sidebar() {
    do_action( 'left_sidebar_after_sidebar' );
}



function left_sidebar_before_content_container() {
    do_action( 'left_sidebar_before_content_container' );
}



function left_sidebar_before_content() {
    do_action( 'left_sidebar_before_content' );
}


if ( function_exists('childtheme_override_left_sidebar_content') )  {
  /**
   * @ignore
   */
   function left_sidebar_content() {
      
      childtheme_override_left_sidebar_content();
    
    }
} else {
  /**
   * Load default left sidebar content
   * 
   * Override: childtheme_override_left_sidebar_content
   * 
   */
    function left_sidebar_content() {
        
        $content = '';
        $content .= '<h1>'.the_title().'</h1>';
        $content .= '<p>Push default left sidebar content</p>';
        echo $content;
  }
}



function left_sidebar_after_content() {
    do_action( 'left_sidebar_after_content' );
}



function left_sidebar_after_content_container() {
    do_action( 'left_sidebar_after_content_container' );
}









/* ----------- Modals ------------- */


/**
 * Register action hook: push_before_modal
 * 
 */
function push_before_modal() {
    
    do_action( 'push_before_modal' );

}



/**
 * Register action hook: push_before_modal_content
 * 
 */
function push_before_modal_content() {
    
    do_action( 'push_before_modal_content' );

}



/**
 * Register action hook: push_modal_content
 * 
 * Load the modal markup template here 
 * 
 */
if ( function_exists('childtheme_override_modal_content') )  {
  /**
   * @ignore
   */
   function push_modal_content() {
      
      childtheme_override_modal_content();
    
    }
} else {
  /**
   * Load default footer content
   * 
   * Override: childtheme_override_modal_content
   * 
   */
    function push_modal_content() {
      
      $content='';
      $content .= '<h3>Modals</h3>';
      $content .= '<p>Push Default Modals content</p>';
      echo $content;
  
  }
}



/**
 * Register action hook: push_after_modal_content
 * 
 */
function push_after_modal_content() {
    
    do_action( 'push_after_modal_content' );

}



/**
 * Register action hook: push_after_modal
 * 
 */
function push_after_modal() {
    
    do_action( 'push_after_modal' );

}







/* ----------- Sitemap ------------- */



function sitemap_before_content_container() {

    do_action('sitemap_before_content_container');

}



function sitemap_before_content() {

    do_action('sitemap_before_content');

}



if ( function_exists('childtheme_override_sitemap_content') )  {
  /**
   * @ignore
   */
   function sitemap_content() {
      
      childtheme_override_sitemap_content();
    
    }
} else {

    function sitemap_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Sitemap Content</p>';
      echo $content;
  }
}



function sitemap_after_content() {

    do_action('sitemap_after_content');

}



function sitemap_after_content_container() {

    do_action('sitemap_after_content_container');

}









/* ----------- FAQ ------------- */



function faq_before_content_container() {

    do_action('faq_before_content_container');

}



function faq_before_content() {

    do_action('faq_before_content');

}



if ( function_exists('childtheme_override_faq_content') )  {
  /**
   * @ignore
   */
   function faq_content() {
      
      childtheme_override_faq_content();
    
    }
} else {

    function faq_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default FAQ content</p>';
      echo $content;
  
  }
}



function faq_after_content() {

    do_action('faq_after_content');

}



function faq_after_content_container() {

    do_action('faq_after_content_container');

}







/* ----------- Blog ------------- */


/**
 * Register action hook: blog_before_get_sidebar
 * 
 */
function blog_before_get_sidebar() {
    do_action( 'blog_before_get_sidebar' );
}



/**
 * Register action hook: blog_get_sidebar
 * 
 * Load the blog sidebar markup template here 
 * 
 */
if ( function_exists('childtheme_override_blog_get_sidebar') )  {
  /**
   * @ignore
   */
   function blog_get_sidebar() {
      
      childtheme_override_blog_get_sidebar();
    
    }
} else {
  /**
   * Load default blog get sidebar
   * 
   * Override: childtheme_override_blog_get_sidebar
   * 
   */
    function blog_get_sidebar() {
      
      get_sidebar();
  
  }
}



/**
 * Register action hook: blog_after_get_sidebar
 * 
 */
function blog_after_get_sidebar() {
    do_action( 'blog_after_get_sidebar' );
}



/**
 * Register action hook: blog_before_content_container
 * 
 */
function blog_before_content_container() {
    do_action( 'blog_before_content_container' );
}



/**
 * Register action hook: blog_before_content
 * 
 */
function blog_before_content() {
    do_action( 'blog_before_content' );
}


/**
 * Register action hook: blog_content
 * 
 * Load the blog content markup template here 
 * 
 */
if ( function_exists('childtheme_override_blog_content') )  {
  /**
   * @ignore
   */
   function blog_content() {
      
      childtheme_override_blog_content();
    
    }
} else {
  /**
   * Load default blog content
   * 
   * Override: childtheme_override_blog_content
   * 
   */
    function blog_content() {
        
        $content = '';
        $content .= '<h1>'.the_title().'</h1>';
        $content .= '<p>Push default blog content</p>';
        echo $content;
  }
}



/**
 * Register action hook: blog_after_content
 * 
 */
function blog_after_content() {
    do_action( 'blog_after_content' );
}



/**
 * Register action hook: blog_after_content_container
 * 
 */
function blog_after_content_container() {
    do_action( 'blog_after_content_container' );
}









/* ----------- Top Navigation ------------- */


/* Top Navigation Sidebar */

function top_navigation_before_sidebar_container() {
    do_action( 'top_navigation_before_sidebar_container' );
}

function top_navigation_before_sidebar() {
    do_action( 'top_navigation_before_sidebar' );
}



if ( function_exists('childtheme_override_top_navigation_sidebar') )  {
  /**
   * @ignore
   */
   function top_navigation_sidebar() {
      
      childtheme_override_top_navigation_sidebar();
    
    }
} else {
  /**
   * Load default top navigation get sidebar
   * 
   * Override: childtheme_override_top_navigation_sidebar
   * 
   */
    function top_navigation_sidebar() {
      
      get_sidebar();
  
  }
}



function top_navigation_after_sidebar() {

    do_action( 'top_navigation_after_sidebar' );

}


function top_navigation_after_sidebar_container() {
    
    do_action( 'top_navigation_after_sidebar_container' );

}




/* Top Navigation Content */


function top_navigation_before_content_container() {

    do_action( 'top_navigation_before_content_container' );

}



function top_navigation_before_content() {

    do_action( 'top_navigation_before_content' );

}



if ( function_exists('childtheme_override_top_navigation_content') )  {
  /**
   * @ignore
   */
   function top_navigation_content() {
      
      childtheme_override_top_navigation_content();
    
    }
} else {
  /**
   * Load default top navigation content
   * 
   * Override: childtheme_override_top_navigation_content
   * 
   */
    function top_navigation_content() {
        
        $content = '';
        echo $content;
  }
}



function top_navigation_after_content() {

    do_action( 'top_navigation_after_content' );

}



function top_navigation_after_content_container() {

    do_action( 'top_navigation_after_content_container' );

}










