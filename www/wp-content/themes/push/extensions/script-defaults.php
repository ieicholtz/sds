<?php
/**
 * Script Defaults
 *
 * @package Extensions
 * @subpackage Script Defaults
 *
 * Functions:
 *
 * push_ie_scripts()
 * push_default_head_scripts()
 * push_default_footer_scripts()
 * google_translate_script()
 * prefix_free_script()
 *
 * Hooks:
 * push_head_scripts()
 * push_footer_scripts()
 *
 */



/* ----- Functions ----- */



/**
 * Load IE Specific Script
 * 
 */
function push_ie_scripts() {
	
    // Register Scripts
    wp_register_script('html5_js', get_bloginfo('template_directory').'/scripts/html5.js' , false, '1.0', true);
    // wp_register_script('modernizr_js', get_bloginfo('template_directory').'/scripts/modernizer.js' , false, '1.0', true);
    
    // Enqueue Scripts
    wp_enqueue_script('html5_js');
    // wp_enqueue_script('modernizr_js');

}



/**
 * Load default head scripts
 * 
 */
function push_default_head_scripts() {
	
    // Register Scripts
    wp_register_script('prefix_js', get_bloginfo('template_directory').'/scripts/prefixfree.min.js' , false, '1.0', true);
    
    // Enqueue Scripts
    wp_enqueue_script('prefix_js');

}



/**
 * Load default footer scripts
 * 
 */
function push_default_footer_scripts() {
    // only use this method is we're not in wp-admin
    if (!is_admin()) {
 
        // deregister the original version of jQuery
        wp_deregister_script('jquery');
 
        // discover the correct protocol to use
        $protocol='http:';
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') {
            $protocol='https:';
        }
 
        // register the Google CDN version
        wp_register_script('jquery', $protocol.'//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', false, '1.9.1', true);
        // wp_register_script('jquery_ui_js', $protocol.'//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js', false, '1.10.1', true);
        // wp_register_script('backbone_js', get_bloginfo('template_directory').'/scripts/backbone.js' , false, '1.0', true);
        // wp_register_script('underscore_js', get_bloginfo('template_directory').'/scripts/underscore.js' , false, '1.0', true);
        wp_register_script('form_js', get_bloginfo('template_directory').'/scripts/jquery.form.min.js' , false, '1.0', true);
        wp_register_script('contact7_js', get_bloginfo('template_directory').'/scripts/contact7.js' , false, '1.0', true);
        wp_register_script('cycle_js', get_bloginfo('template_directory').'/scripts/jquery.cycle.all.js' , false, '1.0', true);
        wp_register_script('css-plugin_js', get_bloginfo('template_directory').'/scripts/CSSPlugin.min.js' , false, '1.0', true);
        wp_register_script('ease-pack_js', get_bloginfo('template_directory').'/scripts/EasePack.min.js' , false, '1.0', true);
        wp_register_script('tween-lite_js', get_bloginfo('template_directory').'/scripts/TweenLite.min.js' , false, '1.0', true);
        wp_register_script('push-ui_js', get_bloginfo('template_directory').'/scripts/push-ui.js' , false, '1.0', true);

        // add it back into the queue
        wp_enqueue_script('jquery');
        // wp_enqueue_script('jquery_ui_js');
        // wp_enqueue_script('underscore_js');
        // wp_enqueue_script('backbone_js');
        wp_enqueue_script('form_js'); 
        wp_enqueue_script('contact7_js');
        wp_enqueue_script('cycle_js');
        wp_enqueue_script('css-plugin_js');
        wp_enqueue_script('ease-pack_js');
        wp_enqueue_script('tween-lite_js');
        wp_enqueue_script('push-ui_js');

        //load prefix free script
        prefix_free_script();
    }
}



/**
 *
 * Load Google Translate Script
 *
 * @since Push 1.0
 *
 */
function google_translate_script(){
    
    $content = '';
    $content .= '<script type="text/javascript">';
    $content .= 'function googleTranslateElementInit() {';
    $content .= 'new google.translate.TranslateElement({pageLanguage: "en", includedLanguages: "de,en,es,fr,pt,zh-CN", layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, "google_translate_element");';
    $content .= '} </script>';
    $content .= '<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>';
    
    echo $content;
}



/**
 *
 * Load Google Map Script
 *
 * @since Push 1.0
 *
 */
function google_map_script($key){
    
    $content = '';
    $content .= "\n";
    $content .= '<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key='.$key.'&sensor=true">';
    $content .= '</script>';

    echo $content;
}



/**
 *
 * Load Prefix Free Script
 *
 * @since Push 1.0
 *
 */
function prefix_free_script(){
    
    $content = '';
    $content .= '<script>';
    $content .= '(function($, self){';
    $content .= 'if(!$ || !self) { return; }';
    $content .= 'for(var i=0; i<self.properties.length; i++) {';
    $content .= 'var property = self.properties[i], camelCased = StyleFix.camelCase(property), PrefixCamelCased = self.prefixProperty(property, true);';
    $content .= '$.cssProps[camelCased] = PrefixCamelCased; }';
    $content .= '})(window.jQuery, window.PrefixFree);';
    $content .= '</script>';
    
    echo $content;
}



/* ----- Hooks ----- */



/**
 * Register action hook: push_head_scripts
 * 
 * Load any site specific head scripts here
 * 
 * 
 */
function push_head_scripts() {
    do_action('push_head_scripts');
} // end push_head_scripts



/**
 * Register action hook: push_footer_scripts
 * 
 * Load any site specific footer scripts here
 * 
 * 
 */
function push_footer_scripts() {
    do_action('push_footer_scripts');
} // end push_footer_scripts









    
    
        

    
        
            
            
    
        
    

     







