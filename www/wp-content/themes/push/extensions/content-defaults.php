<?php
/**
 * Content Defaults
 *
 * @package Extensions
 * @subpackage Content Defaults
 *
 * Hooks:
 * TEMPLATE_before_content_container()
 * TEMPLATE_before_content()
 * TEMPLATE_content()
 * TEMPLATE_after_content()
 * TEMPLATE_after_content_container()
 *
 */



/* ================= Before Content Container ============== */



/**
 * Register action hook: 404_before_content_container
 * 
 */
function push_404_before_content_container() {
    
    do_action( 'push_404_before_content_container' );

}



/**
 * Register action hook: archive_before_content_container
 * 
 */
function archive_before_content_container() {
    
    do_action( 'archive_before_content_container' );

}



/**
 * Register action hook: attachment_before_content_container
 * 
 */
function attachment_before_content_container() {
    
    do_action( 'attachment_before_content_container' );

}



/**
 * Register action hook: author_before_content_container
 * 
 */
function author_before_content_container() {
    
    do_action( 'author_before_content_container' );

}



/**
 * Register action hook: category_before_content_container
 * 
 */
function category_before_content_container() {
    
    do_action( 'category_before_content_container' );

}



/**
 * Register action hook: date_before_content_container
 * 
 */
function date_before_content_container() {
    
    do_action( 'date_before_content_container' );

}



/**
 * Register action hook: push_before_content_container
 * 
 */
function push_before_content_container() {
    
    do_action( 'push_before_content_container' );

}



/**
 * Register action hook: page_before_content_container
 * 
 */
function page_before_content_container() {
    
    do_action( 'page_before_content_container' );

}



/**
 * Register action hook: search_before_content_container
 * 
 */
function search_before_content_container() {
    
    do_action( 'search_before_content_container' );

}


/**
 * Register action hook: sidebar_before_content_container
 * 
 */
function sidebar_before_content_container() {
    
    do_action( 'sidebar_before_content_container' );

}



/**
 * Register action hook: single_before_content_container
 * 
 */
function single_before_content_container() {
    
    do_action( 'single_before_content_container' );

}



/**
 * Register action hook: tag_before_content_container
 * 
 */
function tag_before_content_container() {
    
    do_action( 'tag_before_content_container' );

}



/**
 * Register action hook: taxonomy_before_content_container
 * 
 */
function taxonomy_before_content_container() {
    
    do_action( 'taxonomy_before_content_container' );

}




/* ================= Before Content ==================== */



/**
 * Register action hook: 404_before_content
 * 
 */
function push_404_before_content() {
    
    do_action( 'push_404_before_content' );

}



/**
 * Register action hook: archive_before_content
 * 
 */
function archive_before_content() {
    
    do_action( 'archive_before_content' );

}



/**
 * Register action hook: attachment_before_content
 * 
 */
function attachment_before_content() {
    
    do_action( 'attachment_before_content' );

}



/**
 * Register action hook: author_before_content
 * 
 */
function author_before_content() {
    
    do_action( 'author_before_content' );

}



/**
 * Register action hook: category_before_content
 * 
 */
function category_before_content() {
    
    do_action( 'category_before_content' );

}



/**
 * Register action hook: date_before_content
 * 
 */
function date_before_content() {
    
    do_action( 'date_before_content' );

}



/**
 * Register action hook: push_before_content
 * 
 */
function push_before_content() {
    
    do_action( 'push_before_content' );

}



/**
 * Register action hook: page_before_content
 * 
 */
function page_before_content() {
    
    do_action( 'page_before_content' );

}



/**
 * Register action hook: search_before_content
 * 
 */
function search_before_content() {
    
    do_action( 'search_before_content' );

}



/**
 * Register action hook: sidebar_before_content
 * 
 */
function sidebar_before_content() {
    
    do_action( 'sidebar_before_content' );

}



/**
 * Register action hook: single_before_content
 * 
 */
function single_before_content() {
    
    do_action( 'single_before_content' );

}



/**
 * Register action hook: tag_before_content
 * 
 */
function tag_before_content() {
    
    do_action( 'tag_before_content' );

}



/**
 * Register action hook: taxonomy_before_content
 * 
 */
function taxonomy_before_content() {
    
    do_action( 'taxonomy_before_content' );

}




/* ================= Content ======================== */



/**
 * Register action hook: 404_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_404_content') )  {
  /**
   * @ignore
   */
   function push_404_content() {
      
      childtheme_override_404_content();
    
    }
} else {
  /**
   * Load default 404 content
   * 
   * Override: childtheme_override_404_content
   * 
   */
    function push_404_content() {
      
      $content = '';
      $content .= '<h1>404 Not Found</h1>';
      $content .= '<p>Push Default 404 Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: archive_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_archive_content') )  {
  /**
   * @ignore
   */
   function archive_content() {
      
      childtheme_override_archive_content();
    
    }
} else {
  /**
   * Load default archive content
   * 
   * Override: childtheme_override_archive_content
   * 
   */
    function archive_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Archive Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: attachment_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_attachment_content') )  {
  /**
   * @ignore
   */
   function attachment_content() {
      
      childtheme_override_attachment_content();
    
    }
} else {
  /**
   * Load default attachment content
   * 
   * Override: childtheme_override_attachment_content
   * 
   */
    function attachment_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Attachment Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: author_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_author_content') )  {
  /**
   * @ignore
   */
   function author_content() {
      
      childtheme_override_author_content();
    
    }
} else {
  /**
   * Load default author content
   * 
   * Override: childtheme_override_author_content
   * 
   */
    function author_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Author Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: category_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_category_content') )  {
  /**
   * @ignore
   */
   function category_content() {
      
      childtheme_override_category_content();
    
    }
} else {
  /**
   * Load default category content
   * 
   * Override: childtheme_override_category_content
   * 
   */
    function category_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Category Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: date_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_date_content') )  {
  /**
   * @ignore
   */
   function date_content() {
      
      childtheme_override_date_content();
    
    }
} else {
  /**
   * Load default date content
   * 
   * Override: childtheme_override_date_content
   * 
   */
    function date_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Date Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: push_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_push_content') )  {
  /**
   * @ignore
   */
   function push_content() {
      
      childtheme_override_push_content();
    
    }
} else {
  /**
   * Load default push content
   * 
   * Override: childtheme_override_push_content
   * 
   */
    function push_content() {
      
      $content = '';
      $content .= '<div class="grid-container">';
      $content .= '<h1>'.get_the_title().'</h1>';
      $content .= the_content(); 
      $content .= '</div>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: page_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_page_content') )  {
  /**
   * @ignore
   */
   function page_content() {
      
      childtheme_override_page_content();
    
    }
} else {
  /**
   * Load default page content
   * 
   * Override: childtheme_override_page_content
   * 
   */
    function page_content() {
        
      $content = '';
      $content .= '<div class="grid-container">';
      $content .= '<h1>'.get_the_title().'</h1>';
      $content .= '<div class="default-content">';
      $content .= get_the_content(); 
      $content .= '</div>'; 
      $content .= '</div>'; 
      echo $content;

  }
}



/**
 * Register action hook: search_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_search_content') )  {
  /**
   * @ignore
   */
   function search_content() {
      
      childtheme_override_search_content();
    
    }
} else {
  /**
   * Load default search content
   * 
   * Override: childtheme_override_search_content
   * 
   */
    function search_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Search Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: sidebar_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_sidebar_content') )  {
  /**
   * @ignore
   */
   function sidebar_content() {
      
      childtheme_override_sidebar_content();
    
    }
} else {
  /**
   * Load default sidebar content
   * 
   * Override: childtheme_override_sidebar_content
   * 
   */
    function sidebar_content() {
      
      $content = '';
      $content .= '<h1>Sidebar</h1>';
      $content .= '<p>Push Default Sidebar Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: single_content
 * 
 * Load the single markup template here 
 * 
 */
if ( function_exists('childtheme_override_single_content') )  {
  /**
   * @ignore
   */
   function single_content() {
      
      childtheme_override_single_content();
    
    }
} else {
  /**
   * Load default single content
   * 
   * Override: childtheme_override_single_content
   * 
   */
    function single_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Singles Page</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: tag_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_tag_content') )  {
  /**
   * @ignore
   */
   function tag_content() {
      
      childtheme_override_tag_content();
    
    }
} else {
  /**
   * Load default tag content
   * 
   * Override: childtheme_override_tag_content
   * 
   */
    function tag_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Tag Content</p>'; 
      echo $content;
  
  }
}



/**
 * Register action hook: taxonomy_content
 * 
 * Load the push markup template here 
 * 
 */
if ( function_exists('childtheme_override_taxonomy_content') )  {
  /**
   * @ignore
   */
   function taxonomy_content() {
      
      childtheme_override_taxonomy_content();
    
    }
} else {
  /**
   * Load default links content
   * 
   * Override: childtheme_override_taxonomy_content
   * 
   */
    function taxonomy_content() {
      
      $content = '';
      $content .= '<h1>'.the_title().'</h1>';
      $content .= '<p>Push Default Taxonomy Content</p>'; 
      echo $content;
  
  }
}




/* ================= After Content ================== */


/**
 * Register action hook: 404_after_content
 * 
 */
function push_404_after_content() {
    
    do_action( 'push_404_after_content' );

}



/**
 * Register action hook: archive_after_content
 * 
 */
function archive_after_content() {
    
    do_action( 'archive_after_content' );

}



/**
 * Register action hook: attachment_after_content
 * 
 */
function attachment_after_content() {
    
    do_action( 'attachment_after_content' );

}



/**
 * Register action hook: author_after_content
 * 
 */
function author_after_content() {
    
    do_action( 'author_after_content' );

}



/**
 * Register action hook: category_after_content
 * 
 */
function category_after_content() {
    
    do_action( 'category_after_content' );

}



/**
 * Register action hook: date_after_content
 * 
 */
function date_after_content() {
    
    do_action( 'date_after_content' );

}



/**
 * Register action hook: push_after_content
 * 
 */
function push_after_content() {
    
    do_action( 'push_after_content' );

}



/**
 * Register action hook: page_after_content
 * 
 */
function page_after_content() {
    
    do_action( 'page_after_content' );

}



/**
 * Register action hook: search_after_content
 * 
 */
function search_after_content() {
    
    do_action( 'search_after_content' );

}



/**
 * Register action hook: sidebar_after_content
 * 
 */
function sidebar_after_content() {
    
    do_action( 'sidebar_after_content' );

}



/**
 * Register action hook: single_after_content
 * 
 */
function single_after_content() {
    
    do_action( 'single_after_content' );

}



/**
 * Register action hook: tag_after_content
 * 
 */
function tag_after_content() {
    
    do_action( 'tag_after_content' );

}



/**
 * Register action hook: taxonomy_after_content
 * 
 */
function taxonomy_after_content() {
    
    do_action( 'taxonomy_after_content' );

}




/* ================= After Content Container ============= */


/**
 * Register action hook: 404_after_content_container
 * 
 */
function push_404_after_content_container() {
   
    do_action( 'push_404_after_content_container' );

}



/**
 * Register action hook: archive_after_content_container
 * 
 */
function archive_after_content_container() {
   
    do_action( 'archive_after_content_container' );

}



/**
 * Register action hook: attachment_after_content_container
 * 
 */
function attachment_after_content_container() {
   
    do_action( 'attachment_after_content_container' );

}



/**
 * Register action hook: author_after_content_container
 * 
 */
function author_after_content_container() {
   
    do_action( 'author_after_content_container' );

}



/**
 * Register action hook: category_after_content_container
 * 
 */
function category_after_content_container() {
   
    do_action( 'category_after_content_container' );

}



/**
 * Register action hook: date_after_content_container
 * 
 */
function date_after_content_container() {
   
    do_action( 'date_after_content_container' );

}



/**
 * Register action hook: push_after_content_container
 * 
 */
function push_after_content_container() {
   
    do_action( 'push_after_content_container' );

}



/**
 * Register action hook: page_after_content_container
 * 
 */
function page_after_content_container() {
   
    do_action( 'page_after_content_container' );

}



/**
 * Register action hook: search_after_content_container
 * 
 */
function search_after_content_container() {
   
    do_action( 'search_after_content_container' );

}



/**
 * Register action hook: sidebar_after_content_container
 * 
 */
function sidebar_after_content_container() {
   
    do_action( 'sidebar_after_content_container' );

}



/**
 * Register action hook: single_after_content_container
 * 
 */
function single_after_content_container() {
   
    do_action( 'single_after_content_container' );

}



/**
 * Register action hook: tag_after_content_container
 * 
 */
function tag_after_content_container() {
   
    do_action( 'tag_after_content_container' );

}



/**
 * Register action hook: taxonomy_after_content_container
 * 
 */
function taxonomy_after_content_container() {
   
    do_action( 'taxonomy_after_content_container' );

}




