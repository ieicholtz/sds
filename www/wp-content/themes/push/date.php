<?php
/**
 * Date Template
 *
 * -
 *
 * @package Push
 * @subpackage Page
 *
 */
?>

<?php get_header() ?>

<?php date_before_content_container() ?>

<section class="template-date">

    <?php date_before_content() ?>

        <?php date_content() ?>        

    <?php date_after_content() ?>

</section>

<?php date_after_content_container() ?>

<?php get_footer() ?>
