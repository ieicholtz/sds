<?php
/**
 * Author Template
 *
 * Displays an archive index of posts by a singular Author. 
 * It can display a micrformatted vCard for Author if option is selcted in the default Theme Options.
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>

<?php get_header() ?>

<?php author_before_content_container() ?>

<section class="template-author">

    <?php author_before_content() ?>

        <?php author_content() ?>

    <?php author_after_content() ?>

</section>

<?php author_after_content_container() ?>
	
<?php get_footer() ?>
