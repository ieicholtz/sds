<?php
/**
 * …
 * 
 * @package Push
 * @subpackage Templates
 */
 ?>
    
<?php get_header() ?>

<?php page_before_content_container() ?>

<?php while(have_posts()): the_post() ?>
<section class="template-page">

    <?php page_before_content() ?>

		<?php page_content() ?>

    <?php page_after_content() ?>

</section>
<?php endwhile ?>
<?php page_after_content_container() ?>
   
<?php get_footer() ?>
