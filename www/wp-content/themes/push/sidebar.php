<?php
/**
 * Main Sidebar Template
 *
 * …
 * 
 * @package Push
 * @subpackage Templates
 */
?>

<?php sidebar_before_content_container() ?>

<section class="template-sidebar">

    <?php sidebar_before_content() ?>

        <?php sidebar_content() ?>

    <?php sidebar_after_content() ?>

</section>

<?php sidebar_after_content_container() ?>




