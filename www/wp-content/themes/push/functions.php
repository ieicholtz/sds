<?php
/**
 * Theme Functions
 * 
 * All things that are good
 * 
 * @package Push
 */


/**
 * Registers action hook: push_init 
 * 
 * @since Push 1.0
 */
function push_init() {
	do_action('push_init');
}


/**
 * push_theme_setup & childtheme_override_theme_setup
 *
 * Override: childtheme_override_theme_setup
 *
 * @since push 1.0
 */
if ( function_exists('childtheme_override_theme_setup') ) {
	/**
	 * @ignore
	 */
	function push_theme_setup() {
		childtheme_override_theme_setup();
	}
} else {
	/**
	 * push_theme_setup
	 *
	 * @since Push 1.0
	 */
	function push_theme_setup() {
		/* INCLUDES FOR CUSTOM TEMPLATE */

		// Path constants
		define( 'PUSH_IT', get_template_directory() );
		
		/* ----- Extensions ----- */
		// Load Header Defaults
		require_once ( PUSH_IT . '/extensions/header-defaults.php' );
		// Load Footer Defaults
		require_once ( PUSH_IT . '/extensions/footer-defaults.php' );
		// Load Script Defaults
		require_once ( PUSH_IT . '/extensions/script-defaults.php' );
		// Load Style Defaults
		require_once ( PUSH_IT . '/extensions/style-defaults.php' );
		// Load Content Defaults
		require_once ( PUSH_IT . '/extensions/content-defaults.php' );
		// Load Template Defaults
		require_once ( PUSH_IT . '/extensions/template-defaults.php' );
	
		
		/* ----- Library ----- */
		// Load Post Types
		require_once ( PUSH_IT . '/library/post-types.php' );
		// Load Taxonomy Functions
		require_once ( PUSH_IT . '/library/taxonomy-functions.php' );
		// Load Custom Functions
		require_once ( PUSH_IT . '/library/custom-functions.php' );
		// Load Custom Fields
		require_once ( PUSH_IT . '/library/custom-fields.php' );
        // Load Excerpt Functions
		require_once ( PUSH_IT . '/library/excerpts.php' );
		// Load Shortcodes
		require_once ( PUSH_IT . '/library/shortcodes.php' );
        // Load Widgets
        require_once ( PUSH_IT . '/library/widgets.php');

	}
}

add_action('after_setup_theme', 'push_theme_setup', 10);


/**
 * Registers action hook: push_child_init
 * 
 * @since Push 1.0
 */
function push_child_init() {
	do_action('push_child_init');
}

add_action('after_setup_theme', 'push_child_init', 20);


if ( function_exists('childtheme_override_init_navmenu') )  {
	/**
	 * @ignore
	 */
	 function push_init_navmenu() {
    	childtheme_override_init_navmenu();
    }
} else {
	/**
	 * Register primary nav menu
	 * 
	 * Override: childtheme_override_init_navmenu
	 */
    function push_init_navmenu() {
    	register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'push' ),
			'footer' => __( 'Footer Menu', 'push' ),
		) );
		
	}
}
add_action('init', 'push_init_navmenu');

// Load Custom Post Types 
add_action( 'init', 'load_post_types' );

// Load Custom Taxonomies
add_action( 'init', 'build_taxonomies', 0 );

// Load Custom Widgets & Sidebars
add_action('widgets_init', 'load_widgets');

//Remove CF7 Scripts
remove_action( 'wp_enqueue_scripts', 'wpcf7_enqueue_scripts' );

?>
