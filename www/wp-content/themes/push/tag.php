<?php
/**
 * Tag Archive Template
 *
 * …
 * 
 * @package Push
 * @subpackage Templates
 */
?>

<?php get_header() ?>

<?php tag_before_content_container() ?>

<section class="tag-template">

    <?php tag_before_content() ?>

		<?php tag_content() ?>

    <?php tag_after_content() ?>

</section>

<?php tag_after_content_container() ?>

<?php get_footer() ?>
