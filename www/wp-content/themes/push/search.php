<?php
/**
 * Search Template
 *
 * …
 * 
 * @package Push
 * @subpackage Templates
 */
?>

<?php get_header() ?>

<?php search_before_content_container() ?>

<section class="template-search">

    <?php search_before_content() ?>

		<?php search_content() ?>

    <?php search_after_content() ?>

</section>

<?php search_after_content_container() ?>
    
<?php get_footer() ?>
