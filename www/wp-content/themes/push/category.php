<?php
/**
 * Category Template
 *
 * Displays an archive index of posts assigned to a Category. 
 *
 * @package Push
 * @subpackage Templates
 *
 */
?>

<?php get_header() ?>

<?php category_before_content_container() ?>

<section class="template-category">

    <?php category_before_content() ?>

		<?php category_content() ?>

    <?php category_after_content() ?>

</section>

<?php category_after_content_container() ?>
	
<?php get_footer() ?>
