<?php
/**
 * Index Template
 *
 * This file is required by WordPress to recoginze Push as a valid theme.
 * It is also the default template WordPress will use to display your web site,
 * when no other applicable templates are present in this theme's root directory
 * that apply to the query made to the site.
 * 
 *
 * @package Push
 * @subpackage Templates
 */
?>

<?php get_header() ?>

<?php push_before_content_container() ?>

<section class="template-index">

    <?php push_before_content() ?>

		<?php push_content() ?>

    <?php push_after_content() ?>

</section>

<?php push_after_content_container() ?>
	
<?php get_footer() ?>
