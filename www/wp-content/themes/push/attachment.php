<?php
/**
 * Attachments Template
 *
 * Displays singular WordPress Media Library items.
 *
 * @package Push
 * @subpackage Pages
 *
 */
?>
	
<?php get_header() ?>

<?php attachment_before_content_container() ?>

<section class="template-attachment">

    <?php attachment_before_content() ?>

        <?php attachment_content() ?>

    <?php attachment_after_content() ?>

</section>

<?php attachment_after_content_container() ?>

<?php get_footer() ?>
