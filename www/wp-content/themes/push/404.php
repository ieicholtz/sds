<?php
/**
 * Error 404 Page Template
 *
 * Displays a "Not Found" message and a search form when a 404 Error is encountered.
 *
 * @package Push
 * @subpackage Page
 *
 */
?>

<?php get_header() ?>

<?php push_404_before_content_container() ?>

<section class="template-404">

    <?php push_404_before_content() ?>

        <?php push_404_content() ?>        

    <?php push_404_after_content() ?>

</section>

<?php push_404_after_content_container() ?>

<?php get_footer() ?>
