<?php
/**
 * Push Header Template
 * @package WordPress
 * @subpackage Push
 * @since Push 1.0
 **/
?>

<!DOCTYPE html>

<!--[if lt IE 8]><html <?php language_attributes() ?> class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html <?php language_attributes() ?> class="no-js lt-ie10 lt-ie9 ie8"> <![endif]-->
<!--[if IE 9]><html <?php language_attributes() ?> class="no-js lt-ie10 ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes() ?> class="no-js"> <!--<![endif]-->

<head>
	<!-- meta -->
	<meta charset="utf-8">
	<meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
	<meta content="Ian Eicholtz, Eric Van Holtz" name="author">
    
    <?php push_meta() ?>
    
    <?php push_show_robots() ?>

    <!-- MOBILE -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0">

	<!-- FACEBOOK OPEN GRAPH AND GOOGLE SCHEMA (FOR +1s) -->
	<!-- <meta property="fb:app_id" content="1234567890"> -->
	<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
	<meta property="og:url" content="<?php bloginfo('url') ?>">
	<meta property="og:title" content="<?php bloginfo('name'); ?>">
	<meta property="og:type" content="website">
	<meta property="og:description" content="<?php bloginfo('description') ?>">
	<?php push_facebook_meta() ?>
	
	
	<?php push_additional_meta() ?>

	<!-- title -->
	<?php push_doctitle() ?>

	<!-- styles -->
	<?php push_default_styles() ?>

	<?php push_global_styles() ?>

	<?php push_ie_styles() ?>

	<!-- icons -->
	<?php push_icons() ?>

	<!-- scripts -->
	<?php push_default_head_scripts() ?>

	<?php push_head_scripts() ?>

	<?php if (is_page('contact-us')): ?>
	    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCGZdiGHo3ZBEqmoReDM9TjOW-ylh1YuLw&sensor=false"></script>
		<script type="text/javascript" src="http://www.map-generator.org/map/iframejs/9066c1fd-32b6-425d-b2bf-9051b8e7436d?key=AIzaSyCGZdiGHo3ZBEqmoReDM9TjOW-ylh1YuLw&width=582px&height=320px"></script>
	<?php endif ?>

	<?php wp_head() ?>

</head>

<body <?php body_class() ?>>

<?php push_before_header() ?>

<header>
	
	<?php push_before_header_content() ?>

	<?php push_header_content() ?>

	<?php push_after_header_content() ?>

</header>

<?php push_after_header() ?>	









