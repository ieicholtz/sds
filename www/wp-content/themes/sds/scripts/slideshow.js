/* Slideshow js */

$(function() {
  
  // var sliderImage = $('#slider').find('img').attr('src');
  // $('#featured-slide').attr('src', sliderImage);

  /* Slider */
  function initSlider() {

    initSlide1();

    var currentSlide, slideWidth, sliderPos;
    sliderPos = $(window).width() * .5;
    slideWidth = $('#slider').width();
    currentSlide = 1;

    $("#slider").cycle({
      fx: 'fade',
      slideExpr: 'a.slide',
      slideResize: false,
      speedIn: 1000,
      speedOut: 500,
      timeout: 10000,
      pause: 1,
      next: '#slider-next',
      prev: '#slider-prev',
      pager: '#slider-nav',
      before: function() {
        if($('#slider-content').hasClass('check')) {
          $('#slider-content').removeClass('check');
        } else {
          if(!$('body').hasClass('sliderStopped')) {
            $('#slider-content').stop().fadeOut();
          }
        }
      },
      after: function() {
        if($(this).attr('title') != undefined) {
          $('#slider-content').html('<h1 id="sliderTitle">' + $(this).attr('title') + '</h1>');
          $('#slider-content').stop().fadeIn();
        } else {
          $('#slider-content').stop().hide();
        }
      }
    });

  }

  function initSlide1() {

    $('#slider').delay(10).fadeIn(1500);

    // Van
    setTimeout(function() {
      $('#van').fadeIn();
      TweenLite.to($('#van'), 4, {left: -220});
    }, 3000);

    // Garage 1
    setTimeout(function() {
      $('#garage-1').fadeIn();
      TweenLite.to($('#garage-1'), 1, {height: 84, ease:Linear.easeNone});
    }, 4000);

    // Garage 2
    setTimeout(function() {
      $('#garage-2').fadeIn();
      TweenLite.to($('#garage-2'), 1, {height: 97, ease:Linear.easeNone});
    }, 4500);

    // Overhang
    setTimeout(function() {
      $('#overhang').fadeIn();
      TweenLite.to($('#overhang'), 0.75, {top: 283, ease:Cubic.easeIn});
    }, 5000);

    // Car Dirty
    setTimeout(function() {
      $('#car-dirty').fadeOut(1500);
    }, 7000);

    // Sign
    setTimeout(function() {
      $('#sign').fadeIn();
      TweenLite.to($('#sign'), 0.75, {top: 295, right: -112, ease:Cubic.easeIn});
    }, 5500);

  }

  // Responsive

  if($(window).width() <= 640) {
    $('body').addClass('sliderStopped');
  }

  $(window).resize(function(){
    if($(window).width() <= 640) {
      if(!$('body').hasClass('sliderStopped')) {
        $("#slider").cycle(0);
        $("#slider").cycle('pause');
        $('body').addClass('sliderStopped');
      }
    } else {
      if($('body').hasClass('sliderStopped')) {
        $('body').removeClass('sliderStopped');
        $("#slider").cycle('resume');
      }
    }
  });

  // Initialize Slider
  if($(window).width() > 640) {
    initSlider();
  } else {
    initSlider();
    $("#slider").cycle(0);
    $("#slider").cycle('pause');
  }

  $('#slider a').click(function (event) {
    // event.preventDefault();
  });

  /* End Slider */

});
