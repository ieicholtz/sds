
$(function() {

    $('#showroom').cycle({ 
        fx: 'scrollHorz',
        slideResize: false,
        containerResize: 0,
        next: '#slider-next',
        prev: '#slider-prev',
        pager: '#slider-nav',
        before: onBefore
    });

    function onBefore(curr, next, opts, fwd) {
        var newHeight = $(this).height();
        $(this).parent().animate({
            height: newHeight
        });
    }

});