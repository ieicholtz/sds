/* Core Site js */

$(function() {

    $('#mobile-menu-button').on('click', function() {
        if($('body').hasClass('open')) {
            $(this).html('&#9776;');
            $('body').removeClass('open');
            $('#mobile-menu').slideUp();
        } else {
            $(this).text('X');
            $('body').addClass('open');
            $('#mobile-menu').slideDown();
        }
    });

    $('#mobile-cover').on('click', function() {
        $('#mobile-menu-button').html('&#9776;');
        $('body').removeClass('open');
        $('#mobile-menu').slideUp();
    });

});