<?php
/**
 * 404 Template
 *
 * @uses push/404.php
 *
*/
?>

<section id="template-404">

    <?php if(get_field('404_image', 'options')): ?>
        <div id="hero-error">
                <?php $image = get_field('404_image', 'options') ?>
            <a href="<?php the_field('404_link', 'options') ?>" target="<?php the_field('404_link_type', 'options') ?>" title="Back to Homepage" />
                <img src="<?php echo $image ?>" />
            </a>
        </div>
    <?php endif ?>

    <div class="grid-container copy-error">
        <div class="grid-100">
            <h1>404 - Page not found.</h1>
        </div>

        <div class="grid-20 prefix-40 suffix-40">

            <?php the_field('404_text', 'options') ?>

                <a class="ui-button" href="/" target="_self" />Go Home</a>

        </div>

    </div>

</section>
