<?php
/**
 * Footer Template
 *
 * @package footer.php
 *
*/
?>

<?php if (get_field('callouts', 'options')): ?>
    <div id="footer-callouts" class="grid-container">
        <?php $callouts = get_field('callouts', 'options') ?>
        <?php $i = 1 ?>
        <?php foreach ($callouts as $callout): ?>
            <div id="footer-callout-<?php echo $i ?>" class="footer-callout grid-30">
                <?php if ($callout['image']): ?>
                    <img src="<?php echo $callout['image'] ?>" />
                <?php endif ?>
                <div class="footer-callout-info">
                    <h2><?php echo $callout['title'] ?></h2>
                    <p><?php echo $callout['text'] ?></p>
                    <div class="grid-60 suffix-40">
                        <a class="ui-button" href="<?php echo $callout['link'] ?>" title="<?php echo $callout['link_text'] ?>"><?php echo $callout['link_text'] ?></a>
                    </div>
                </div>
            </div>
            <?php $i++ ?>
        <?php endforeach ?>
    </div>
<?php endif ?>

<div id="sub-footer">
    <div class="grid-container">
        <div class="grid-80 mobile-grid-80">
            <nav><?php wp_nav_menu(array( 'theme_location'  => 'footer' )) ?></nav>
            <p id="copyright"><?php the_field('copyright', 'options') ?> &copy; <?php echo date('Y') ?></p>
        </div>
        <div class="social-nav grid-20 mobile-grid-20">
            <ul>
                <li>
                    <a href="<?php the_field('twitter_link', 'options') ?>" target="_blank">
                        <img src="<?php echo get_bloginfo('stylesheet_directory').'/images/logo_twitter.png' ?>" alt="Twitter"/>
                    </a>
                </li>
                <li>
                    <a href="<?php the_field('facebook_link', 'options') ?>" target="_blank">
                        <img src="<?php echo get_bloginfo('stylesheet_directory').'/images/logo_facebook.png' ?>" alt="Facebook"/>
                    </a>                    
                </li>
            </ul>
        </div>
    </div>
</div>

