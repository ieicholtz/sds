<?php
/**
  * Template Blog Category
  *
  */
?>

<?php if (have_posts() ): ?>
    <section class="template-blog-category blog-subpage">
        <div id="blog-container" class="grid-container">

            <div class="grid-25 grid-parent">
                <?php get_template_part('templates/blog-sidebar') ?>
            </div>

            <div class="grid-parent grid-70 prefix-5">
            
                <h1>Topic: <span id="category-title"><?php echo single_cat_title( '', false ) ?></span></h1>
                
                <?php while ( have_posts() ) : the_post() ?>
                    <article class="blog-post">
                        <div class="grid-parent grid-100 blog-title">
                            <h2>"<?php the_title() ?>"</h2>
                        </div>
                        <div class="grid-parent grid-100 blog-date">
                            <?php $postDate = get_the_date('F d, Y') ?>
                            <span class="post-date"><?php echo $postDate ?></span>
                        </div>

                        <div class="grid-parent grid-100 blog-excerpt">
                            <p><?php echo  trim_excerpt(get_the_excerpt(), 200) ?><a class="read-more" href="<?php the_permalink() ?>" alt="<?php the_title() ?>">Read More</a></p>
                        </div>

                    </article>
                <?php endwhile ?>

            </div>

        </div>
    </section>
<?php endif ?>
