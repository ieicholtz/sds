<?php
/**
 * Sitemap Template
 *
 * @uses push/sitemap.php
 *
*/
?>

<?php // load template specific styles
    wp_register_style('sitemap_css', get_bloginfo('stylesheet_directory').'/styles/includes/sitemap.css');
    wp_enqueue_style('sitemap_css');
?>

<h1><?php the_title() ?>

<div class="main-content">

    <?php the_content() ?>

</div>

<div id="sitemap">
    
    <a href="/">Demo Sitemap</a>
    <?php wp_nav_menu( array( 'theme_location' => 'primary', 'container' => '')); ?>

</div>
