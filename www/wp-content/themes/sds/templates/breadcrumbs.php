<?php
/**
 * Breadcrumbs Template
 *
*/
?>

<section class="template-breadcrumbs">
    <div class="page-top grid-100">
        <div class="grid-container">
           
            <?php breadcrumbs() ?>

        </div><!--grid-container-->
    </div><!--page-top-->
</section>
