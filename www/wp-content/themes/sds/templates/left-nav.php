<?php
/**
 * Left Nav Template
 *
*/
?>

<?php $nav = get_field('nav') ?>
<nav><?php wp_nav_menu( array( 'theme_location' => $nav, 'container' => '')) ?></nav>

