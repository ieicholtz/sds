<?php
/**
 * Slideshow Template
 *
 * @uses push/home.php
 *
*/
?>

<?php if (has_post_thumbnail()): ?>
    <?php
        // $domsxe = simplexml_load_string(get_the_post_thumbnail());
        // $thumbnailsrc = $domsxe->attributes()->src;
    ?>
<?php endif ?>

<section id="template-slideshow" class="hide-on-mobile">

    <div id="slider">

        <div id="slider-content-container">
            <a id="slider-prev" href="javascript:void(0);" target="_self"></a>
            <a id="slider-next" href="javascript:void(0);" target="_self"></a>
            <div class="grid-container">
                <div id="slider-content" class="check"></div>
            </div>
        </div>

        <?php $slides = get_field('slideshow') ?>
        
        <?php $i=1 ?>

        <?php foreach ($slides as $slide): ?>

            <?php if ($i == 1): ?>

                <a id="slide-<?php echo $i ?>" class="slide" style="background-image: url('<?php echo $slide['image'] ?>');" href="<?php echo $slide['link'] ?>" target="<?php echo $slide['link_type'] ?>" title="<?php echo $slide['title'] ?>">

                    <div class="grid-container">

                        <div id="van"></div>

                        <div id="car-dirty"></div>
                        <div id="car-clean"></div>

                        <div id="sign"></div>

                        <div id="overhang"></div>

                        <div id="garage-1"></div>
                        <div id="garage-2"></div>

                    </div>

                </a>

            <?php else: ?>
                <a id="slide-<?php echo $i ?>" class="slide" style="background-image: url('<?php echo $slide['image'] ?>');" href="<?php echo $slide['link'] ?>" target="<?php echo $slide['link_type'] ?>" title="<?php echo $slide['title'] ?>"></a>
            <?php endif ?>

            <?php $i++ ?>

        <?php endforeach ?>

        <div id="slider-nav"></div>

    </div>

</section>
