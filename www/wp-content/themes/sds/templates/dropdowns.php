<?php
/**
 * Dropdown Nav Template
 *
 * @package header.php
 *
*/
?>

<section class="template-dropdowns">
    <div class="dropdown-container grid-parent grid-container">
        <div class="grid-33">
            <?php wp_nav_menu( array( 'theme_location' => 'about', 'container' => '')); ?>
        </div>
        <div class="grid-33">
            <?php wp_nav_menu( array( 'theme_location' => 'services', 'container' => '')); ?>
        </div>

    </div>
</section>

