<?php
/**
 * Single Product Nav Template
 *
*/
?>
<?php 
    $args = array( 'post_type' => 'product-service', 'posts_per_page' => '-1', 'order' => 'ASC' );

    if (isset($_GET['parent'])) :
        $parentPost =  get_post($_GET['parent']);
        $parentID = $parentPost->post_parent;
        $postsList = array();

        query_posts(array('orderby' => 'menu-order', 'order'=> 'ASC', 'showposts' => '20', 'post_parent' => $parentID, 'post_type' => 'page')); while (have_posts()) : the_post();
            Global $post;
            $data = array(
                'title'=>get_the_title(),
                'name'=>$post->post_name,
                'id'=>$post->ID,
                'link'=>get_permalink(),
            );
            array_push($postsList, $data);
            $data = array();

        endwhile;
        wp_reset_query();
    ?>
        <section class="template-single-product-nav">
            <nav><ul>
                <?php foreach($postsList as $list) : ?>
                    <?php if ($list['id'] == $parentPost->ID): ?>
                        <li class="current"><a href="<?php echo $list['link'] ?>" title="<?php echo $list['title'] ?>"><?php echo $list['title'] ?></a></li>
                        <nav class="side-sub-nav"><?php generate_postType_nav($args, $list['name'].'-tax', $list['name'], 'products-and-services', $list['id'] ) ?></nav> 
                    <?php else: ?>
                        <li><a href="<?php echo $list['link'] ?>" title="<?php echo $list['title'] ?>"><?php echo $list['title'] ?></a></li>
                    <?php endif ?>
                <?php endforeach ?>
            </ul></nav>
        </section>
    
    <?php else: ?>
       <?php $catSlug = get_custom_tax_slug(get_the_ID(), 'products-and-services') ?>
       <?php $postSlug = $post->post_name ?>
            <section class="template-single-product-nav">
               <div class="single-product-nav"> 
                    <nav><?php generate_postType_nav($args, $catSlug, $postSlug, 'products-and-services') ?></nav>
               </div>
            </section>
   <?php endif ?>

