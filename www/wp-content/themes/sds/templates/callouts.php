<?php
/**
 * Callouts Template
 *
 * @uses push/home.php
 *
*/
?>

<?php if(get_field('callouts')): ?>
<?php $callouts = get_field('callouts') ?>
    <section class="callouts">
        <div class="grid-parent grid-container grid-100">
            <div class="callouts-title">
                <h3>Features</h3>
            </div>
            <div class="grid-parent grid-100 callouts-container">
                <?php $i=1 ?>
                <?php foreach($callouts as $callout): ?>
                    <div class="grid-parent grid-33 mobile-grid-100 callout-item callout-<?php echo $i ?>">
                        <div class="callout-pad">
                            <a href="<?php echo $callout['link'] ?>" target="<?php echo $callout['link_type'] ?>" title="Read More">
                                <img class="img-border" src="<?php echo $callout['image'] ?>" alt="<?php echo $callout['title'] ?>" />
                            </a>
                            <a class="hide-on-mobile btn-arrow-right" href="<?php echo $callout['link'] ?>" target="<?php echo $callout['link_type'] ?>" title="Read More"></a>
                            <h2 class="hide-on-mobile callout-title"><?php echo $callout['title'] ?></h2>
                             <a class="hide-on-desktop" href="<?php echo $callout['link'] ?>" target="<?php echo $callout['link_type'] ?>" title="Read More"><h2 class="callout-title"><?php echo $callout['title'] ?><span class="btn-arrow-right"></span></h2></a>
                            <p class="hide-on-mobile"><?php echo $callout['link_text'] ?></p>
                        </div>
                    </div>
                    <?php $i++ ?>
                <?php endforeach ?>
            </div>
        </div>
    </section>
<?php endif ?>
