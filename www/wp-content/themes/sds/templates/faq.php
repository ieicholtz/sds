<?php
/**
 * FAQ Include
 *
 * @uses push/faq.php
 *
*/
?>

<?php // load template specific styles
    wp_register_style('faq_css', get_bloginfo('stylesheet_directory').'/styles/includes/faq.css');
    wp_enqueue_style('faq_css');
?>

<h1><?php the_title() ?></h1>

<div class="main-content">

    <?php the_content() ?>

</div>

<div id="faq-questions">
    <?php $questions = get_field('questions') ?>
    <?php if($questions): ?>
        <?php foreach($questions as $question): ?>
            <div class="faq-item">
                <div class="faq-item-container">
                    <a href="javascript:void(0)" class="faq-accordion-link"><?php echo $question['question'] ?><span class="faq-accordion-arrow"></span></a>
                    <div class="faq-accordion-description">
                        <p><?php echo $question['answer'] ?></p>
                    </div><!-- .faq-accordion-description -->
                </div><!-- .faq-item-container -->
            </div><!-- .faq-item -->
        <?php endforeach ?>
    <?php endif ?>
</div>
<div class="clear"></div>
