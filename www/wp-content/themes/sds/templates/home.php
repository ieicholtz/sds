<?php
/**
 * Home Template
*/
?>

<section id="template-home" class="grid-container hide-on-mobile">
    <div class="grid-45 suffix-5">
        <h1 class="hide-on-mobile"><?php the_field('title') ?></h1>
        <div class="default-content">
            <?php the_content() ?>
        </div>
        <blockquote>"<?php the_field('quote') ?>"</blockquote>
        <p class="author">- <?php the_field('quote_author') ?></p>
    </div>
    <div class="grid-50 mobile-grid-100">
        <?php if (get_field('image')): ?>
            <img id="home-photo" src="<?php the_field('image') ?>" title="SDS Mobile Detail" />
        <?php endif ?>
    </div>
</section>
