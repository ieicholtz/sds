<?php
/**
 * Header Template
 *
 * @package header.php
 *
*/
?>

<header class="grid-container">
    <div id="header-wrapper" class="grid-100 mobile-grid-100">
        <div id="mobile-menu" class="hide-on-desktop">
            <nav>
                <?php wp_nav_menu(array( 'theme_location'  => 'primary' )) ?>
            </nav>
        </div>
        <?php if (get_field('logo', 'options')): ?>
            <?php $image = get_field('logo', 'options') ?>
            <a id="logo-sds" href="<?php bloginfo('url') ?>" alt="<?php bloginfo('name') ?>"><img src="<?php echo $image ?>" alt="<?php bloginfo('name') ?>" /></a>
        <?php endif ?>
        <a id="mobile-menu-button" class="hide-on-desktop" title="Menu">&#9776;</a>
        <div id="header-nav" class="hide-on-mobile">
            <nav>
                <?php wp_nav_menu(array( 'theme_location'  => 'primary' )) ?>
            </nav>
            <div class="social-nav">
                <ul>
                    <li>
                        <a href="<?php the_field('facebook_link', 'options') ?>" target="_blank">
                            <img src="<?php echo get_bloginfo('stylesheet_directory').'/images/logo_facebook.png' ?>" alt="Facebook"/>
                        </a>
                    </li>
                    <li>
                        <a href="<?php the_field('twitter_link', 'options') ?>" target="_blank">
                            <img src="<?php echo get_bloginfo('stylesheet_directory').'/images/logo_twitter.png' ?>" alt="Twitter"/>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>

<div id="mobile-cover"></div>