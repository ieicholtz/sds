<?php
/**
 * Top Nav Template
 *
*/
?>

<section class="template-top-nav">
    <div class="sub-nav">
        
        <div class="hide-on-desktop grid-parent mobile-grid-100 sub-menu">
            <a class="sub-menu-reveal"><?php the_title() ?> <span class="sub-menu-icon"></span></a>
        </div>

        <div class="grid-parent grid-container">

            <?php if (is_singular('product-service')) : ?>
            
                <?php $nav = 'product-nav' ?>

            <?php else: ?>
                <?php $nav = get_field('nav_menu') ?>
                
            <?php endif ?>
            <nav class="center-sub-nav">
                <?php wp_nav_menu( array( 
                    'theme_location' => $nav, 
                    'container'   => '', 
                    'menu_class'  => 'menu clearfix' ,
                )) ?>
            </nav>

        </div><!--grid-container-->
    </div>
</section>
