<?php
/**
  * Template Name: Redirect
  *
  */

if (is_page('products-services')) {
    wp_redirect( bloginfo('url')."/products-services/mill-products-paper-services/" );
    exit;
}

if (is_page('corporate-overview')) {
    wp_redirect( bloginfo('url')."/corporate-overview/history/" );
    exit;
}


?>
