<?php
/**
 * Template Blog-detail
 *
 *
*/
?>


<section class="template-blog-detail blog-subpage">
    <div id="blog-container" class="grid-container">

        <div class="grid-parent grid-25">
            <?php get_template_part('templates/blog-sidebar') ?>
        </div>
        
        <div class="grid-parent grid-70 prefix-5">

            <article class="blog-post blog-post-full">

                <div class="grid-parent grid-100 blog-title">
                    <h2>"<?php the_title() ?>"</h2>
                </div>

                <div class="grid-parent grid-100 blog-date">
                    <?php $postDate = get_the_date('F d, Y') ?>
                    <span class="post-date"><?php echo $postDate ?></span>
                </div>

                <div class="grid-parent grid-100">
                    <p><?php echo the_content(); ?></p>
                </div>

            </article>
            
            <div class="nav">
                <nav id="nav-below" class="navigation">
                    <?php $prev = get_previous_post() ?>
                    <?php $next = get_next_post() ?>
                    <?php previous_post_link( '%link', __( 'Previous', 'push') ) ?>
                    <?php if ($prev && $next): ?>
                        <span class="divider"> | </span>
                    <?php endif ?>
                    <?php next_post_link( '%link', __( 'Next', 'push') ) ?>
                </nav>
            </div>

        </div>

    </div>
</section>


