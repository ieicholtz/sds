<?php
/**
 * Template Blog-sidebar
 *
 *
*/
?>
<section id="template-blog-sidebar">
        <div class="sidebar-widget grid-100">
            <ul>
                <?php dynamic_sidebar('blog-sidebar-widget') ?>
            </ul>
        </div>
</section>
