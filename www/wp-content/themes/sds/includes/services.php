<?php
/**
 * Include Services
 *
 */
?>

<?php
$serviceItems = array();
$args = array( 'post_type' => 'service', 'orderby' => 'menu_order', 'posts_per_page' => '-1', 'order' => 'ASC' );
$the_query = new WP_Query( $args );
while ( $the_query->have_posts() ) { $the_query->the_post();
     $serviceSection = get_custom_tax_name($post->ID, 'service_type');
     $array = array(
        'name' => get_the_title(),
        'section' => $serviceSection,
        'desc' => get_the_content(),
        'features' => get_field('features'),
        'price' => get_field('price'),
     );
    if (is_page('46')):
        if ($serviceSection == 'Exterior'):
            $serviceItems[$post->ID] = $array;
        endif;
    elseif (is_page('48')):
        if ($serviceSection == 'Interior'):
            $serviceItems[$post->ID] = $array;
        endif;
    elseif (is_page('53')):
        if ($serviceSection == 'Other'):
            $serviceItems[$post->ID] = $array;
        endif;
    else:
        $serviceItems[$post->ID] = $array;
    endif;
    $array = array();
};

wp_reset_postdata(); 
?>


<section id="include-services">
    <div class="grid-container">
        <div id="services-intro" class="grid-parent grid-100"></div>

        <?php foreach ($serviceItems as $service): ?>

            <div class="service grid-parent grid-70 suffix-10">
                <div class="grid-parent grid-100">
                    <h1><?php echo $service['name'] ?></h1>
                </div>

                <?php if ($service['features']): ?>
                <?php $features = $service['features'] ?>
            
                    <div class="grid-parent grid-100">
                        <ul>
                            <?php foreach ($features as $feature): ?>
                                <li><?php echo $feature['feature'] ?></li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                <?php endif ?>

                <div class="grid-100 grid-parent">
                    <!-- <a class="read-description" href="#">Read Description</a> -->
                    <div class="service-description">
                        <?php echo $service['desc'] ?>
                    </div>
                </div>
            </div>

            <div class="service-pricing grid-20">
                <?php if ($service['price']): ?>
                    <p class="starting">Starting at</p>
                    <p class="price"><?php echo $service['price'] ?></p>
                <?php endif ?>
                <a class="appointment ui-button" href="/contact-us" title="Schedule Appointment">Schedule Appointment</a>
            </div>

        <?php endforeach ?>
    </div>
</section>
