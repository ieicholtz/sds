<?php
/**
 * Include Video
 *
 */
?>

<?php wp_enqueue_style('video-css') ?>

<?php $video = formatVideo(get_field('video')) ?>

<div class="include-video">
    <iframe class="video-border" width="450" height="320" src="<?php echo $video ?>" frameborder="0" allowfullscreen></iframe>
</div>

