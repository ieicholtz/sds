<?php
/**
 * Default Template
 *
 *
 *
 */
?>

<section id="include-default">
    <div class="grid-container">
        <div class="grid-100">
            <h1><?php the_title() ?></h1>
            <?php if (the_content()): ?>
            <div class="content-info">
                <?php the_content() ?>
            </div>
            <?php endif ?>
        </div>
    </div>
</section>
