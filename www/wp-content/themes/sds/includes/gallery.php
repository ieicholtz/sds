<?php
/**
 * Include Gallery
 *
 */
?>
<section class="include-gallery">
    <?php $gallery = get_field('gallery') ?>
    <?php if($gallery): ?>
        <div class="grid-container">
            <div id="slider-content-container">
                <a id="slider-prev" href="javascript:void(0);" target="_self"></a>
                <a id="slider-next" href="javascript:void(0);" target="_self"></a>
            </div>
            <div id="showroom">
                    <?php foreach ($gallery as $image): ?>
                        <img src="<?php echo $image['image'] ?>" alt="<?php $image['title'] ?>" />
                    <?php endforeach ?>
            </div>
            <div id="slider-nav"></div>
        </div>
    <?php else: ?>
        <h2>Coming soon...</h2>
    <?php endif ?>
</section>
