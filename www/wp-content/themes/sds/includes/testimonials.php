<?php
/**
 * Testimonials Include
 *
 */
?>

<?php if (get_field('testimonials')): ?>

    <?php $testimonials = get_field('testimonials') ?>

    <section id="include-testimonials">

        <?php foreach ($testimonials as $test): ?>

            <div class="testimonial-quote grid-parent grid-100">
                <blockquote><?php echo $test['quote'] ?></blockquote>
            </div>

            <div class="grid-parent grid-100">
                <p class="testimonial-author">- <?php echo $test['author'] ?>. <?php echo $test['author_info'] ?></p>
            </div>

        <?php endforeach ?>

    </section>

<?php endif ?>

