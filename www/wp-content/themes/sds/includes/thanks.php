<?php
/**
 * Include Thanks
 *
 */
?>

<section id="includes-thanks">
    <div class="grid-100">
        <div class="default-content">
            <?php the_field('thanks_text') ?>
        </div>
    </div>
</section>
