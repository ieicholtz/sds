<?php
/**
 * Include Team
 *
 */
?>

<?php if (get_field('team')): ?>

    <?php $teams = get_field('team') ?>

    <section id="include-team">

        <?php $i = 1; ?>

        <?php foreach ($teams as $team): ?>

            <div id="team-member-<?php echo $i; ?>" class="team-member grid-parent grid-100">

                <div class="grid-parent grid-75 prefix-5 push-20">
                    <h2><?php echo $team['name'] ?></h2>
                    <p class="team-member-title"><?php echo $team['title'] ?></p>
                    <div class="default-content">
                        <?php echo $team['description'] ?>
                    </div>
                    <?php if ($team['quote']): ?>
                        <blockquote>"<?php echo $team['quote'] ?>"</blockquote>
                    <?php endif ?>
                </div>

                <div class="team-member-photo grid-parent grid-20 pull-80">
                    <?php if ($team['image']): ?>
                        <img src="<?php echo $team['image'] ?>" alt="<?php echo $team['name'] ?>" />
                    <?php endif ?>
                </div>

            </div>

            <?php $i++; ?>

        <?php endforeach ?>

    </section>

<?php endif ?>
