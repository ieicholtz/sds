<?php
/**
 * Include Contact Us
 *
 */
?>

<section id="includes-contact-us" class="grid-parent grid-100">

    <div id="contact-us-info">
        <h1><?php the_title() ?></h1>
        <div id="contact-us-address">
            <?php the_content() ?>
        </div>
        <p><strong>Phone:</strong> <?php the_field('phone') ?></p>
        <p><strong>Fax:</strong> <?php the_field('fax') ?></p>
        <p><strong>Email:</strong> <?php the_field('email') ?></p>
    </div>

    <div id="contact-us-map">
        
        <div id="mapid-9066c1fd-32b6-425d-b2bf-9051b8e7436d"></div>
        
    </div>

</section>
