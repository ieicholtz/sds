<?php
/**
 * Include Blog
 *
 *
*/
?>
<section id="include-blog">
    <div id="blog-intro" class="grid-100">
        <h1><?php the_title() ?></h1>
        <?php the_content() ?>
    </div>

    <?php $page = get_query_var('page') ? get_query_var('page') : 1 ?>
    <?php $args = array( 'post_type' => 'post', 'paged' => $page, 'orderby' => 'post_date' ) ?>
    <?php $the_query = new WP_Query( $args ) ?>
    <?php while ( $the_query->have_posts() ) : $the_query->the_post() ?>

        <article class="blog-post grid-parent grid-100">
            <div class="grid-parent grid-100 blog-title">
                <h2>"<?php the_title() ?>"</h2>
            </div>

            <div class="blog-date grid-parent grid-100">
                <?php $postDate = get_the_date('F d, Y') ?>
                <span class="post-date"><?php echo $postDate ?></span>
            </div>
            
            <div class="blog-excerpt grid-parent grid-100">
                <p><?php echo trim_excerpt(get_the_excerpt(), 200) ?></p>
                <a class="read-more" href="<?php the_permalink() ?>" alt="<?php the_title() ?>">Read More</a>
            </div>
        </article>

    <?php endwhile ?>

    <?php $links = paginate_links(array(
        'base'     => '/news/page/%#%',
        'format'   => '/news/page/%#%',
        'current'  => $page,
        'total'    => $the_query->max_num_pages,
        'type'     => 'array',
        'show_all' => true,
        'prev_next' => false,
    )) ?>

    <?php wp_reset_query() ?>

    <?php if($links): ?>
        <div class="grid-parent pagination">
            <div class="grid-15 mobile-grid-33">
            <?php if (1 !== $page): ?>
                <a href="/news/page/<?php echo ($page - 1) ?>" class="btn-arrow-left"></a>
            <?php endif ?>
            </div>

            <div class="grid-70 mobile-grid-33 numbers">Page <?php echo $links[$page - 1] ?> of <?php echo $links[$the_query->max_num_pages - 1] ?></div>
            <div class="grid-15 mobile-grid-33">
            <?php if ($the_query->max_num_pages != $page): ?>
                <a href="/news/page/<?php echo ($page + 1) ?>" class="btn-arrow-right"></a>
            <?php endif ?>
            </div>                
        </div>
    <?php endif ?>

</section>
