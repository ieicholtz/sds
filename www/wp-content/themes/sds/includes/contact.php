<?php
/**
 * Include Contact
 *
 */
?>

<section id="includes-contact" class="grid-parent grid-100">

    <div class="grid-parent grid-100">

        <h3><?php the_field('contact_title') ?></h3>

        <?php $form = get_field('contact_form') ?>

        <?php if ($form == 'testimonials'): ?>
            <?php echo do_shortcode('[contact-form-7 id="4" title="Testimonials"]') ?>
        <?php elseif ($form == 'contact'): ?>
            <?php echo do_shortcode('[contact-form-7 id="75" title="Contact Us"]') ?>
        <?php endif ?>

</section>
