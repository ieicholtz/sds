<?php
/**
 * Footer Functions
 *
 * @package Functions
 * @subpackage Footer Functions
 *
 */




/**
*
* Define the main content tmeplate for the Footer
*
* */
function childtheme_override_footer_content() {
	
	get_template_part('templates/footer');

}
add_action('push_footer_content', 'childtheme_override_footer_content');



/**
*
* Define any template to be loaded after main footer
*
* */
function childtheme_after_footer() {
	
    //load Google Translate Script
    //google_translate_script();

    //load Google Map Script
    //google_map_script('AIzaSyDayAScr56jaOQG6wC-3Wsi2SzD0dnowWs');

}
add_action('push_after_footer', 'childtheme_after_footer' );



/**
*
* Define Site Specific global Scripts
*                      
*/
function childtheme_footer_scripts() {
	
	// only use this method is we're not in wp-admin
    if (!is_admin()) {
    	
    	wp_deregister_script( 'contact-form-7' );
		wp_register_script('core-js', get_bloginfo('stylesheet_directory').'/scripts/core.js' , false, '1.0', true);
        wp_register_script('slideshow-js', get_bloginfo('stylesheet_directory').'/scripts/slideshow.js' , false, '1.0', true);
        // wp_register_script('callouts-js', get_bloginfo('stylesheet_directory').'/scripts/callouts.js' , false, '1.0', true);
        // wp_register_script('infobubble-js', get_bloginfo('stylesheet_directory').'/scripts/infobubble.js' , false, '1.0', true);
        // wp_register_script('stellar-js', get_bloginfo('stylesheet_directory').'/scripts/stellar.js' , false, '1.0', true);
        // wp_register_script('easing-js', get_bloginfo('stylesheet_directory').'/scripts/jquery.easing.min.js' , false, '1.0', true);
        wp_register_script('custom-form-js', get_bloginfo('stylesheet_directory').'/scripts/custom-form.js' , false, '1.0', true);
        // wp_register_script('waypoints-js', get_bloginfo('stylesheet_directory').'/scripts/waypoints.min.js' , false, '1.0', true);
        // wp_register_script('responsiveslider-js', get_bloginfo('stylesheet_directory').'/scripts/responsiveslides.min.js' , false, '1.0', true);
        wp_register_script('showroom-js', get_bloginfo('stylesheet_directory').'/scripts/showroom.js' , false, '1.0', true);

		//load scripts
        // wp_enqueue_script('stellar-js');
		wp_enqueue_script('core-js');
        // wp_enqueue_script('callouts-js');

        if (is_front_page()) {
            wp_enqueue_script('slideshow-js');
        }

        function has_parent($post, $post_id) {
            if ($post->ID == $post_id) return true;
            else if ($post->post_parent == 0) return false;
            else return has_parent(get_post($post->post_parent),$post_id);
        }

        global $wp_query;
        if(has_parent(($wp_query->post), 16)) {
            wp_enqueue_script('showroom-js');
        }

	}
}
add_action('push_footer_scripts', 'childtheme_footer_scripts' );
