<?php
/**
 * Content Functions
 *
 * @package Functions
 * @subpackage Content Functions
 * 
 */

/**
*
* Define the main content template for the home content
*                    
*/
function childtheme_override_home_content() {
    
    get_template_part('templates/home');

}
add_action('home_content', 'childtheme_override_home_content');



/**
*
* Define the main content template for the slideshow
*                    
*/
function childtheme_override_home_slideshow() {
    
    get_template_part('templates/slideshow');

}
add_action('home_slideshow', 'childtheme_override_home_slideshow');



/**
*
* Define the main content template for the callouts
*                    
*/
function childtheme_override_home_callouts() {
    
    //get_template_part('templates/callouts');

}
add_action('home_callouts', 'childtheme_override_home_callouts');



/**
*
* Define the main content for the basic template 
*                    
*/
function childtheme_override_basic_content() {
    
   if(get_field('includes')){
        $includes = get_field('includes');
        foreach($includes as $include){
            $inc = $include['include'];
            get_template_part('includes/'.$inc);
        }
    };

}
add_action('basic_content', 'childtheme_override_basic_content');


/**
*
* Define Left Sidebar Sidebar 
*                    
*/
function childtheme_override_left_sidebar_sidebar() {
    
    if(get_field('nav')){
        get_template_part('templates/left-nav');
    };

}
add_action('left_sidebar_sidebar', 'childtheme_override_left_sidebar_sidebar');


/**
*
* Define the main content for the Left Sidebar template 
*                    
*/
function childtheme_override_left_sidebar_content() {
    
   if(get_field('includes')){
        $includes = get_field('includes');
        foreach($includes as $include){
            $inc = $include['include'];
            get_template_part('includes/'.$inc);
        }
    };

}
add_action('left_sidebar_content', 'childtheme_override_left_sidebar_content');



/**
*
* Define the main content for the Blog template 
*                    
*/
function childtheme_override_blog_content() {
    
   if(get_field('includes')){
        $includes = get_field('includes');
        foreach($includes as $include){
            $inc = $include['include'];
            get_template_part('includes/'.$inc);
        }
    };

}
add_action('blog_content', 'childtheme_override_blog_content');



/**
*
* Define the main content for the Single template 
*                    
*/
function childtheme_override_single_content() {
    
    get_template_part('templates/blog-detail');

}
add_action('single_content', 'childtheme_override_single_content');



/**
*
* Define Blog Sidebar 
*                    
*/
function childtheme_override_blog_get_sidebar() {
    
    get_template_part('templates/blog-sidebar');

}
add_action('blog_get_sidebar', 'childtheme_override_blog_get_sidebar');



/**
*
* Define the main content for the Category template 
*                    
*/
function childtheme_override_category_content() {
    
    get_template_part('templates/blog-category');

}
add_action('category_content', 'childtheme_override_category_content');



/**
*
* Define the main content for the Date template 
*                    
*/
function childtheme_override_date_content() {
    
    get_template_part('templates/blog-archive');

}
add_action('date_content', 'childtheme_override_date_content');



/**
*
* Define the main content template for the 404 Page
*/
function childtheme_override_404_content() {
    
    get_template_part('templates/404');

}
add_action('push_404_content', 'childtheme_override_404_content');





