<?php
/**
 * Header Functions
 *
 * @package Functions
 * @subpackage header Functions
 *
 * Site specific header Filters and Functions
 * 
 * 
 */


/**
 *
 * Define Site Specific Meta
 * Filter: push_meta
 *                      
 */
function childtheme_meta($content) {
    
    $content = '<meta name="site-name" content="SDS Mobile Detailing">';
    $content .= "\r\n";
    $content .= '<meta name="keywords" content="" >';
    $content .= "\r\n";
    $content .= '<meta name="description" content="" >';
    $content .= "\r\n";
    $content .= '<meta name="google-translate-customization" content="cf29b5ec26d24710-1e3ecff75fc33a39-g48ca856e5c3aa671-e"></meta>';
    
    return $content;

}
add_filter('push_meta', 'childtheme_meta');



/**
 *
 * Define Site Specific Facebook Meta
 *                      
 */
function childtheme_facebook_meta($content) {
	
	$content = '<meta content="';
	$content .= get_bloginfo( 'stylesheet_directory' ).'/assets/images/logo.png';
	$content .= '" property="og:image">';
 	
 	return $content;

}
//add_filter('push_facebook_meta', 'childtheme_facebook_meta');




/* ----- Functions ----- */



/**
*
* Define Site Specific Global Styles
*                      
*/
function childtheme_global_styles() {
	
	//register styles
    //root
    wp_register_style('core_css', get_bloginfo('stylesheet_directory').'/styles/core.css');
    // wp_register_style('grey-screen-css', get_bloginfo('stylesheet_directory').'/styles/grey-screen.css');
    // templates
    wp_register_style('slideshow-css', get_bloginfo('stylesheet_directory').'/styles/templates/slideshow.css');
    wp_register_style('callouts-css', get_bloginfo('stylesheet_directory').'/styles/templates/callouts.css');
	// wp_register_style('modal-css', get_bloginfo('stylesheet_directory').'/styles/templates/modal.css');
    wp_register_style('404-css', get_bloginfo('stylesheet_directory').'/styles/templates/404.css');
    wp_register_style('home-css', get_bloginfo('stylesheet_directory').'/styles/templates/home.css');

    // includes
    wp_register_style('gallery-css', get_bloginfo('stylesheet_directory').'/styles/includes/gallery.css');
    wp_register_style('video-css', get_bloginfo('stylesheet_directory').'/styles/includes/video.css');
    wp_register_style('services-css', get_bloginfo('stylesheet_directory').'/styles/includes/services.css');
    wp_register_style('team-css', get_bloginfo('stylesheet_directory').'/styles/includes/team.css');
    wp_register_style('default-css', get_bloginfo('stylesheet_directory').'/styles/includes/default.css');
    wp_register_style('blog-css', get_bloginfo('stylesheet_directory').'/styles/includes/blog.css');
    wp_register_style('testimonials-css', get_bloginfo('stylesheet_directory').'/styles/includes/testimonials.css');
    wp_register_style('contact-css', get_bloginfo('stylesheet_directory').'/styles/includes/contact.css');
    wp_register_style('contact-us-css', get_bloginfo('stylesheet_directory').'/styles/includes/contact-us.css');
    wp_register_style('thanks-css', get_bloginfo('stylesheet_directory').'/styles/includes/thanks.css');


	//enqueue styles
    wp_enqueue_style('core_css');
	// wp_enqueue_style('modal-css');
	// wp_enqueue_style('grey-screen-css');
	wp_enqueue_style('callouts-css');

    if (is_front_page()) {
        wp_enqueue_style('slideshow-css');
        wp_enqueue_style('home-css');
    }

    if (is_404()) {
        wp_enqueue_style('404-css');
    }

    if (is_singular('post') || is_category() || is_date()) {
        wp_enqueue_style('blog-css');
    }
     
    if (get_field('includes')) {
        $includes = get_field('includes');
        foreach ($includes as $inc) {
            $style = $inc['include'];
            wp_enqueue_style($style.'-css');
        }
    };

 
}
add_action('push_global_styles', 'childtheme_global_styles' );



/**
*
* Define Site Specific IE Styles
*                      
*/
// function childtheme_ie_styles() {
	
	//register styles
	// wp_register_style('ie6_css', get_bloginfo('stylesheet_directory').'/styles/ie/ie6.css');
	// wp_register_style('ie7_css', get_bloginfo('stylesheet_directory').'/styles/ie/ie7.css');
	// wp_register_style('ie8_css', get_bloginfo('stylesheet_directory').'/styles/ie/ie8.css');
	// wp_register_style('ie9_css', get_bloginfo('stylesheet_directory').'/styles/ie/ie9.css');
	// wp_register_style('ie_css', get_bloginfo('stylesheet_directory').'/styles/ie/ie.css');
	//setup conditionals
	// global $wp_styles;
	// $wp_styles->add_data('ie6_css', 'conditional', 'lte IE 6');
	// $wp_styles->add_data('ie7_css', 'conditional', 'IE 7');
	// $wp_styles->add_data('ie8_css', 'conditional', 'IE 8');
	// $wp_styles->add_data('ie9_css', 'conditional', 'IE 9');
	// $wp_styles->add_data('ie_css', 'conditional', 'IE');
	//enqueue styles
	// wp_enqueue_style('ie6_css');
	// wp_enqueue_style('ie7_css');
	// wp_enqueue_style('ie8_css');
	// wp_enqueue_style('ie9_css');
	// wp_enqueue_style('ie_css');

// }
// add_action('push_ie_styles', 'childtheme_ie_styles' );



/**
* Define the main content template for the header
*                      
*/
function childtheme_override_header_content() {
	
	get_template_part('templates/header');

}
add_action('push_header_content', 'childtheme_override_header_content');


// function childtheme_after_header_content() {
    
//     get_template_part('templates/dropdowns');

// }
// add_action('push_after_header_content', 'childtheme_after_header_content');



function childtheme_after_header() {
    
    get_template_part('templates/modal');

}
add_action('push_after_header', 'childtheme_after_header');

?>
