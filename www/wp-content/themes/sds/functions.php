<?php
/**
 * SDS Theme Functions
 *
 *
 * @package sds
 * @subpackage ThemeInit
 */

/*
 * Define constant for child theme name
 */
define( 'THEME_NAME', 'sds' );

/**
 * Define theme setup
 */
function childtheme_setup() {
	
	// add the_post_thumbnail() wherever thumbnail should appear
	add_theme_support( 'post-thumbnails' );
	
	register_nav_menus( array(
        'about' => __( 'About Nav', 'push' ),
        'services' => __( 'Services Nav', 'push' ),
        'showroom' => __( 'Showroom Nav', 'push' ),
	) );
}
add_action('push_child_init', 'childtheme_setup');

// change Search Form input type 
function childtheme_search_form ( $form ) {
	$form = '<form role="search" name="search-form" action="' . home_url( '/' ) . '" method="get">
       <input type="text" placeholder="Search" name="s" id="search-input" />
       <input type="submit" value="" id="search-button" />
    </form>';
	
	return $form;
}
add_filter( 'get_search_form', 'childtheme_search_form' );

/* INCLUDES FOR CHILD TEMPLATE */

// Path constants
define( 'CHILD_URL', get_stylesheet_directory() );

/* ----- Extensions ----- */

// Load Header Functions
require_once ( CHILD_URL . '/functions/header-functions.php' );
// Load Footer Functions
require_once ( CHILD_URL . '/functions/footer-functions.php' );
// Load Content Functions
require_once ( CHILD_URL . '/functions/content-functions.php' );

function custom_init() {
    

}
//add_action('init', 'custom_init');


//add_action( 'wpcf7_before_send_mail', 'change_submit' );

function change_submit( $data )
{
}






